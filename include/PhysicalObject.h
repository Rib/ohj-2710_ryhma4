#ifndef OBJECT_H
#define OBJECT_H

#include <tr1/memory>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <DrawableObject.h>

using std::tr1::shared_ptr;

class PhysicalObject : public DrawableObject
{
    public:
        PhysicalObject();
        virtual ~PhysicalObject();

        virtual void init(); //= 0;

        virtual void update(); // = 0;

        virtual bool destroy() = 0;

   //     getObjectSprite()->setPosition(position_); }

        //checks if object intersect
        bool intersect (shared_ptr<PhysicalObject>& object);

        virtual void collide(sf::Vector2f velocity, unsigned int mass);

        virtual unsigned int getDamage () { return damage_; }
        unsigned int getMass() { return mass_; }

        void setDamage( unsigned int damage ) { damage_ = damage; }
        void setMass ( unsigned int mass ) { mass_ = mass; }

        virtual void doDamage( unsigned int damage) { damage_ = damage_ >= damage ? damage_-damage : 0; }

        virtual sf::Vector2f getDirection() { return sf::Vector2f(0,0); }
        virtual float getSpeed() { return 0; }
        virtual void actForce(sf::Vector2f force) {}

        unsigned int getDistanceLeft() { return distance_left_; }
        void setDistanceLeft (unsigned int distance_left) { distance_left_ = distance_left;}

        void reduceDistance() { if (distance_left_ > 0) --distance_left_; }

    protected:
    private:

    //damage of the object
    unsigned int damage_;
    //mass of the object
    unsigned int mass_;

    unsigned int distance_left_;

    PhysicalObject(const PhysicalObject& other);
    PhysicalObject& operator=(const PhysicalObject& other);

};

#endif // OBJECT_H
