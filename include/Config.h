

#ifndef CONFIG_HH
#define CONFIG_HH

#include <SFML/System/Vector2.hpp>

// per how many pixels there are stars
const unsigned int STARDENSITY = 50000;
//amount of clouds
const unsigned int CLOUDS = 100;


const sf::Vector2u AREA(20000, 2000);

const float FRICTION_FAC = 0.95;
const float ENGINEPOWER = 50;

const sf::Vector2f STARTPOSITION(200,200);
const sf::Vector2f HEALTHBARSIZE(25,100);

const unsigned int SHIPMAXSPEED = 10;

const unsigned int PLAYER_MASS = 2;
const unsigned int ENEMY_BASIC_SHIP_MASS = 4;
const unsigned int ENEMY_BOSS_MASS = 10;

const unsigned int ENEMYSTART = 2500;
const unsigned int AIRANGE = 1400;
const unsigned int AIGROUNDAVOIDANCE = 400;

const unsigned int FIRST_LEVEL_ENEMIES = 50;
const unsigned int SECOND_LEVEL_ENEMIES = 20;
const unsigned int ENEMYENGINEPOWER = 50;

//weapon config
const unsigned int PROJECTILE_DAMAGE = 30;
const unsigned int PROJECTILE_DISTANCE = 100;
const unsigned int PROJECTILESPEED = 15;

const unsigned int ROCKET_DAMAGE = 140;
const unsigned int ROCKET_DISTANCE = 2000;

const unsigned int LASER_DAMAGE = 1;
const unsigned int LASER_DISTANCE = 10;
const unsigned int LASERSPEED = 50;

const unsigned int PLASMABALL_DAMAGE = 50;


const unsigned int SHIP_HULL_HEALTH = 150;
const unsigned int SHIP_ARMOR_HEALTH = 150;
const unsigned int SHIP_SHIELD_HEALTH = 100;
const unsigned int SHIP_GRAV_STAB_HEALTH = 100;
const unsigned int SHIP_ENERGY = 100;


const unsigned int ENEMY_SUBSYSTEM_HEALTH = 10;

const unsigned int ENEMY_BASIC_SHIP_HULL_HEALTH = 10;
const unsigned int ENEMY_BASIC_SHIP_ARMOR_HEALTH = 5;
const unsigned int ENEMY_BASIC_SHIP_SHIELD_HEALTH = 0;
const unsigned int ENEMY_BASIC_SHIP_GRAV_STAB_HEALTH = 5;
const unsigned int ENEMY_BASIC_SHIP_ENERGY = 100;



const unsigned int ENEMY_ROCKET_SHIP_SHIELD_HEALTH = 10;
const unsigned int ENEMY_ROCKET_SHIP_ENERGY = 10;

const unsigned int ENEMY_BOSS_SHIP_HULL_HEALTH = 1000;
const unsigned int ENEMY_BOSS_SHIP_ARMOR_HEALTH = 500;
const unsigned int ENEMY_BOSS_SHIP_SHIELD_HEALTH = 500;
const unsigned int ENEMY_BOSS_SHIP_GRAV_STAB_HEALTH = 500;
const unsigned int ENEMY_BOSS_SHIP_ENERGY = 400;

//higher means slower
const unsigned int HULL_REGENERATION_RATE = 300;
const unsigned int ARMOR_REGENERATION_RATE = 150;

const unsigned int SHIELD_REGENERATION_RATE = 6;

const unsigned int STABILIZER_REGENERATION_RATE = 150;
const unsigned int ENERGY_REGENERATION_RATE = 2;



// tulee olla int, muuten sotkee
const int ENEMYACCURACY  = 50;

const unsigned int BOSSZONE = 1000;

const unsigned int WIDTH = 1920;
//const unsigned int WIDTH = 1024;
//const unsigned int WIDTH = 800;

const unsigned int HEIGHT = 1080;
//const unsigned int HEIGHT = 800;
//const unsigned int HEIGHT = 600;


const std::string Subsystem_name[] =  {"Hull", "Armor", "Shield", "Stabilizer", "Energy", "Weapon"};

const std::string Weapon_textures[] = {"textures/plasmaball.png", "textures/rocket.png", "textures/laser2.png"};

const float PI = 3.14159265;

#endif // CONFIG_HH
