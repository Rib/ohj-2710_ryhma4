#ifndef ANIMATION_H
#define ANIMATION_H

#include <DrawableObject.h>


class Animation : public DrawableObject
{
    public:
        Animation(unsigned int images, unsigned int frameTime);
        virtual ~Animation();

        virtual void loadTexture(const std::string& filename);

        // called from animationmanager
        bool animationUpdate();

    private:

    unsigned int frames_;
    unsigned int currentFrame_;
    unsigned int frameTime_;
    unsigned int counter_;

    sf::Vector2i position_;
    sf::Vector2i size_;


    Animation(const Animation& other);
    Animation& operator=(const Animation& other);
};

#endif // ANIMATION_H
