#ifndef OBJECTFACTORY_H
#define OBJECTFACTORY_H

#include "PhysicalObject.h"

#include <tr1/memory>

#include <map>


#include <Projectile.h>
#include <Level.h>
#include <Ship.h>
#include <HealthBar.h>
#include <StaticObject.h>
//#include <HumanController.h>

class HumanController;
class AiController;
class Controller;
class Ship;
class HealthBar;


class ObjectFactory
{
public:

    virtual ~ObjectFactory();

    void init();
    void cleanup();

    static ObjectFactory* inst();

    shared_ptr<std::map<unsigned int, shared_ptr<DrawableObject> > > getDrawableObjects() const
    {
        return drawable_objects_;
    }

    shared_ptr<std::map<unsigned int, shared_ptr<PhysicalObject> > > getMovingObjects() const { return moving_objects_; }

    //shared_ptr<std::map<unsigned int, shared_ptr<DrawableObject> > > getStaticObjects() const { return static_objects_; }

    shared_ptr<std::vector<shared_ptr<Controller> > > getControllers() const
    {
        return controllers_;
    }

    shared_ptr<HealthBar> getHealthBar() { return health_bar_; }

    //object creation functions

    shared_ptr<Projectile> createProjectile(sf::Vector2f from, sf::Vector2f to);
    void createRocket(sf::Vector2f position, Movement& m, bool left);
    void createLaser(sf::Vector2f from, sf::Vector2f to);
    void createPlasma(sf::Vector2f from, sf::Vector2f to);

    void createGame();
    //void createLevel(Level* level);
    void createHuman();

    shared_ptr<AiController> createEnemy(std::string texture, float scale, bool boss = false);

    shared_ptr<Ship> createShip(bool is_human = false);

    shared_ptr<StaticObject> createGround();

    //object destruction functions
    void destroyObject(shared_ptr<DrawableObject> object);

    bool update();

    shared_ptr<HumanController> getPlayer()
    {
        if(!player_)
        {
            createGame();
        }
        return player_;
    }

    shared_ptr<Ship> getPlayerShip();


    void armShip(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship);
    void armRocket(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship);
    void armLaser(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship);
    void armPlasma(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship);
protected:
    ObjectFactory();

private:
    static ObjectFactory* instance_;
    bool first_;

    shared_ptr<Level> level_;

    unsigned int projectile_;
    unsigned int rocket_;
    unsigned int laser_;
    unsigned int plasma_;

    //TODO transform objects vector into map to get destructions much faster
    shared_ptr<std::map<unsigned int, shared_ptr<DrawableObject> > > drawable_objects_;
    shared_ptr<std::map<unsigned int, shared_ptr<PhysicalObject> > > moving_objects_;
    shared_ptr<std::map<unsigned int, shared_ptr<PhysicalObject> > > static_objects_;

    shared_ptr<HealthBar> health_bar_;

    shared_ptr<HumanController> player_;
    //shared_ptr<Ship> human_ship_;

    shared_ptr<std::vector<shared_ptr<Controller> > > controllers_;

    ObjectFactory(const ObjectFactory& other);
    ObjectFactory& operator=(const ObjectFactory& other);
};

#endif // OBJECTFACTORY_H
