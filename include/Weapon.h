#ifndef WEAPON_H
#define WEAPON_H

#include <iostream>
#include <Subsystem.h>
#include <ObjectFactory.h>
#include <SFML/Window.hpp>

//class ObjectFactory;

enum WeaponType { PROJECTILE = 0, ROCKET = 1, LASER = 2, PLASMA = 3};

class Ship;

class Weapon: public Subsystem
{
    public:
        Weapon(bool left = true);
        virtual ~Weapon();

        void init(shared_ptr<Ship> ship, WeaponType type,
                  unsigned int clipSize,
                  unsigned int shotDelay,
                  unsigned int loadingTime,
                  unsigned int sound);

        void shoot(sf::Vector2f target_pos);
        void update();

        sf::Vector2f getShipPosition();
        shared_ptr<Ship> getShip() { return ship_; }

        unsigned int getLoadingTimeRemaining() { return loading_time_remaining_; }
        unsigned int getLoadingTime() { return loading_time_;}

        unsigned int getClipLeft() { return clipLeft_;}

        void setWeaponType(WeaponType type) { type_ = type; }
        WeaponType getWeaponType() { return type_; }

        sf::Vector2f shootLoc();

        bool isShooting(){ return shoot_ || wasShooting_;}

    protected:
    private:

    void shooting();


    bool shoot_;
    sf::Vector2f target_;
        //ship in which this subsystem is
        shared_ptr<Ship> ship_;
        unsigned int clipSize_;
        unsigned int clipLeft_;

        unsigned int shotDelay_;
        unsigned int delayLeft_;

        //loading time for weapons
        unsigned int loading_time_;
        unsigned int loading_time_remaining_;

        float stepAngle_;
        float currentAngle_;

        bool wasShooting_;
        bool lastDir_;

        WeaponType type_;

        bool left_; // for rocket direction

        unsigned int sound_;

        Weapon(const Weapon& other);
        Weapon& operator=(const Weapon& other);
};

#endif // WEAPON_H
