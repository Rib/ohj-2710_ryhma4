#ifndef HULL_H
#define HULL_H

#include <Subsystem.h>
#include <Ship.h>

class Hull : public Subsystem
{
    public:
        Hull();
        virtual ~Hull();
        void update();
    protected:
    private:
        Hull(const Hull& other);
        Hull& operator=(const Hull& other);
};

#endif // HULL_H
