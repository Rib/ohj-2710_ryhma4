#ifndef SHIELD_H
#define SHIELD_H

#include <Subsystem.h>


class Shield : public Subsystem
{
    public:
        Shield();
        virtual ~Shield();
        void update();
        void doDamage(unsigned int damage);
        void disable(bool disable) { disabled_ = disable; }
        bool isDisabled() { return disabled_;}
    protected:
    private:
        bool disabled_;

        Shield(const Shield& other);
        Shield& operator=(const Shield& other);
};

#endif // SHIELD_H
