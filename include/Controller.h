#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <tr1/memory>
//#include <Ship.h>
class Ship;

class Controller
{
    public:
        Controller();
        virtual ~Controller();

        virtual void control() = 0;

        void setShip(std::tr1::shared_ptr<Ship> ship) { controlled_ = ship; }

        std::tr1::shared_ptr<Ship> getShip() {return controlled_;}

    protected:



    private:

    std::tr1::shared_ptr<Ship> controlled_;

    Controller(const Controller& other);
    Controller& operator=(const Controller& other);
};

#endif // CONTROLLER_H
