#ifndef SHIP_H
#define SHIP_H

#include <iostream>
#include <string>
#include <MovingObject.h>
#include <vector>
#include <ObjectFactory.h>
#include <Graphics.h>
#include "AnimationManager.h"
#include <Weapon.h>

class ObjectFactory;
//class Weapon;

using std::string;
using std::vector;

enum Direction {NONE = 0, UP = 1, UPRIGHT = 2, RIGHT = 3,
                LEFT= 7, UPLEFT = 8, DOWN = 5, DOWNRIGHT = 4, DOWNLEFT = 6,
               };

class Ship : public MovingObject
{
public:
    Ship();
    virtual ~Ship();

    void init();

    //void init(shared_ptr<ObjectFactory> object_factory);

    virtual void update();

    virtual bool destroy()
    {
        AnimationManager::inst()->playAnimation("textures/explosion.png",
                                                getPosition(),
                                                5, 10);
        return true;
    }

    void drawHealthBar();

    void move(Direction dir);

    void aim(int x, int y);
    void shoot(bool  is_shooting, sf::Vector2f target_pos);

    //void setCurrentWeapon(unsigned int weapon) { current_weapon_ = weapons_.size() > weapon ? weapon : weapons_.size()-1;}

    void addWeapon(shared_ptr<Weapon>);

    //void addSubsystem(Subsystem system);


    void setEnginePower(unsigned int power)
    {
        enginePower_ = power;
    }
    unsigned int getMovementPower() const { return enginePower_; }

    vector<shared_ptr<Subsystem> >& getSubSystems()
    {
        return subSystems_;
    }

    vector<shared_ptr<Weapon> >& getWeapons()
    {
        return weapons_;
    }

    unsigned int getCurrentWeapon()
    {
        return current_weapon_;
    }
    void setCurrentWeapon(unsigned int current_weapon)
    {
        current_weapon_ = current_weapon;
    }

    void doDamage(unsigned int damage);
    unsigned int getDamage ()
    {
        return subSystems_.size() > 0 ? subSystems_.at(0)->getHealth():0;
    }

    unsigned int getReloadTimeRemaining ();
    unsigned int getReloadTime ();

    void draw(sf::RenderWindow& window, double interpolation);
protected:

private:
    void movement();

    Direction moving_;

    float enginePower_;

    unsigned int energy_level_;
    unsigned int max_energy_level_;

    vector<shared_ptr<Subsystem> > subSystems_;

    vector<shared_ptr<Weapon> > weapons_;

    shared_ptr<sf::Sprite> shield_halo_;
    sf::Texture shield_halo_texture_;

    //TODO probably should be enum
    // probably shouldn't be enum
    unsigned int current_weapon_;

    Ship(const Ship& other);
    Ship& operator=(const Ship& other);
};

#endif // SHIP_H
