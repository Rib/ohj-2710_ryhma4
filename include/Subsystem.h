#ifndef SUBSYSTEM_H
#define SUBSYSTEM_H

#include <tr1/memory>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

using std::tr1::shared_ptr;

class Ship;

class Subsystem
{
    public:
        Subsystem();
        virtual ~Subsystem();

        void setHealth(unsigned int health) {
            health_ = health;
            max_health_ = health;}
        unsigned int getHealth() { return health_; }
        unsigned int getMaxHealth() { return max_health_; }

        void repair(){repairing_ = true;}
        virtual void doDamage (unsigned int damage);

        virtual void update();
        void draw();

        virtual void disable(bool disable) {}
        virtual bool isDisabled() { return false;}

        Ship* getShip() {return attachedTo_;}
        void setShip(Ship* attach) { attachedTo_ = attach;}

    protected:
        void assignHealth(unsigned int health) { health_ = health;}
    private:
        //heath of the subsystem
        unsigned int health_;
        unsigned int max_health_;

        bool repairing_;

        Ship* attachedTo_;

        Subsystem(const Subsystem& other);
        Subsystem& operator=(const Subsystem& other);
};

#endif // SUBSYSTEM_H
