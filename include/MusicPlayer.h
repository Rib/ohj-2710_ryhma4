#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <SFML/Audio.hpp>

#include <vector>

class MusicPlayer
{
    public:
        MusicPlayer();
        ~MusicPlayer();

        static MusicPlayer* inst();

        void loadMusic();

        void playMusic();

    protected:
    private:

    std::vector<sf::Music> background_;

    static MusicPlayer* instance_;


    MusicPlayer(const MusicPlayer& other);
    MusicPlayer& operator=(const MusicPlayer& other);
};

#endif // MUSICPLAYER_H
