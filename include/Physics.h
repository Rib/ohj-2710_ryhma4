#ifndef PHYSICS_H
#define PHYSICS_H

#include <vector>
#include <tr1/memory>

#include "PhysicalObject.h"

using std::tr1::shared_ptr;
using std::vector;

class Physics
{
    public:
        Physics();
        virtual ~Physics();

        //initializations
        //void init(vector<shared_ptr<Object> >& objects);
        void init();

        //checks and acts on intersections
        void checkIntersections ();

        //iterates the damage through all the objects
        void checkDamage();

        void moveObjects();

    protected:
    private:

    unsigned int explosionID_;
    unsigned int collisionID_;
        //all the objects
        //shared_ptr<vector<shared_ptr<Object> > > objects_;

};

#endif // PHYSICS_H
