#ifndef ROCKET_H
#define ROCKET_H

#include <MovingObject.h>
#include "AnimationManager.h"

class Rocket : public MovingObject
{
    public:
        Rocket();
        virtual ~Rocket();

        virtual bool destroy()
        {
            AnimationManager::inst()->playAnimation("textures/explosion.png",
                                                    getPosition(),
                                                    5, 10);
            return true;
        }

        virtual void init(bool left);

        virtual void update();

        virtual void draw(sf::RenderWindow& window, double interpolation);

    protected:
    private:
    sf::Vector2f force_;

    bool left_;
    unsigned int smoke_;
    unsigned int smokeLeft_;

    Rocket(const Rocket& other);
    Rocket& operator=(const Rocket& other);
};

#endif // ROCKET_H
