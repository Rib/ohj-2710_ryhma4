#ifndef HUMANCONTROLLER_H
#define HUMANCONTROLLER_H

#include <Controller.h>
#include <Graphics.h>

#include <tr1/memory>

class HumanController : public Controller
{
    public:
        HumanController();
        virtual ~HumanController();

        void control();

        bool wantsToQuit();

    private:

    bool shieldKeyDown_;
    bool switchKeyDown_;
    bool wantsToQuit_;

    void movement();

    void shooting();

    HumanController(const HumanController& other);
    HumanController& operator=(const HumanController& other);
};

#endif // HUMANCONTROLLER_H
