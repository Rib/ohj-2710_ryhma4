#ifndef HEALTHBAR_H
#define HEALTHBAR_H

#include <ObjectFactory.h>
#include <DrawableObject.h>
#include <Config.h>

#include <string>

enum Subsystem_type { HULL = 0, ARMOR, SHIELD, STABILIZER, ENERGY, WEAPON};

class HealthBar : public DrawableObject
{
    public:
        HealthBar();
        virtual ~HealthBar();

        void init(unsigned int ship_id);

        void draw();
    protected:
    private:
        HealthBar(const HealthBar& other);
        HealthBar& operator=(const HealthBar& other);

        void loadWeaponTexture(const std::string& filename, unsigned int weapon);

        //id of the ship this healthbar belongs to
        unsigned int ship_id_;

        shared_ptr<sf::RectangleShape> health_bar_;

        std::vector<shared_ptr<sf::Text> > weapon_text_;
        std::vector<shared_ptr<sf::Sprite> > weapon_pictures_;
        std::vector<shared_ptr<sf::Texture> > weapon_textures_;

        shared_ptr<sf::RectangleShape> selected_weapon_;

        std::vector<shared_ptr<sf::Text> > sub_system_text_;
        std::vector<shared_ptr<sf::RectangleShape> > sub_system_bar_;
        std::vector<shared_ptr<sf::RectangleShape> > red_bar_;

        shared_ptr<sf::Text> reload_text_;
        shared_ptr<sf::RectangleShape> reload_bar_;
};

#endif // HEALTHBAR_H
