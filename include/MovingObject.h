#ifndef MOVINGOBJECT_H
#define MOVINGOBJECT_H

#include "PhysicalObject.h"

struct Movement
{
    // normalized vector
    sf::Vector2f direction;
    float speed;
};

class MovingObject : public PhysicalObject
{
public:
    MovingObject();
    virtual ~MovingObject();

    virtual void update() = 0;

    virtual void draw(sf::RenderWindow& window, double interpolation);

    virtual void collide(sf::Vector2f velocity, unsigned int mass);

    //sf::Vector2f getVelocity() { return velocity_; }
    void setMovement(Movement& movement)
    {
        moving_ = movement;
    }

    //sf::Vector2f getAcceleration() { return acceleration_; }
    void setGravity(float strenght){gravity_ = strenght;}

    sf::Vector2f getDirection() { return moving_.direction; }

    virtual float getSpeed() { return moving_.speed; }

    // if this is not called the object will slow down
    void actForce(sf::Vector2f force);

    // strength is how strong the gravity is

    void setMaxSpeed(float maximum)
    {
        maxSpeed_ = maximum;
    }

    float getMaxSpeed() { return maxSpeed_;}

    Movement& getMovement(){return moving_;}

protected:

    void setAirResistance(float resistance)
    {
        airResistance_ = resistance;
    }

private:

    //sf::Vector2f convert(Movement m);


    //sf::Vector2f direction_;
    Movement moving_;

    float airResistance_;
    float maxSpeed_;

    bool force_;

    float gravity_;
    sf::Vector2f accelerationStep_;
    unsigned int steps_;
};

#endif // MOVINGOBJECT_H
