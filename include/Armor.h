#ifndef ARMOR_H
#define ARMOR_H

#include <Subsystem.h>

class Armor : public Subsystem
{
    public:
        Armor();
        virtual ~Armor();
        void update();

    protected:
    private:
        Armor(const Armor& other);
        Armor& operator=(const Armor& other);
};

#endif // ARMOR_H
