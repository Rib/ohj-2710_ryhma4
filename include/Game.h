#ifndef GAME_H
#define GAME_H

#include <Graphics.h>
#include "Physics.h"
#include "ObjectFactory.h"

class Game
{
    public:
        Game();
        virtual ~Game();

        void init(long int seed);
        void cleanup();

        bool stillGoing();

        void update();
        void draw(float interpolation);

    private:

    Physics physics_;

    shared_ptr<HumanController> player_;

    std::vector<Controller*> controllers_;

    shared_ptr<sf::Sprite> crosshair_;

    Game(const Game& other);
    Game& operator=(const Game& other);
};

#endif // GAME_H
