#ifndef GRAVITATIONALSTABILIZER_H
#define GRAVITATIONALSTABILIZER_H

#include <Subsystem.h>


class GravitationalStabilizer : public Subsystem
{
    public:
        GravitationalStabilizer();
        virtual ~GravitationalStabilizer();
        void update();

    protected:
    private:



            GravitationalStabilizer(const GravitationalStabilizer& other);
        GravitationalStabilizer& operator=(const GravitationalStabilizer& other);
};

#endif // GRAVITATIONALSTABILIZER_H
