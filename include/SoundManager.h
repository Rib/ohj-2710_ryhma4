#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <string>

#include <SFML/Audio.hpp>
class SoundManager
{
    public:
        SoundManager();
        ~SoundManager();

        static SoundManager* inst();

        unsigned int loadSound(std::string file);

        void playSound(unsigned int id);

        void playMusic(std::string file);

    protected:
    private:

    static SoundManager* instance_;

    sf::Music music_;
    std::vector<sf::Sound*> sounds_;

    std::vector<sf::SoundBuffer*> buffers_;

    SoundManager(const SoundManager& other);
    SoundManager& operator=(const SoundManager& other);
};

#endif // SOUNDMANAGER_H
