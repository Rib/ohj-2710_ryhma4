#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <cmath>
#include <iostream>

#include <MovingObject.h>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

class Projectile: public MovingObject
{
    public:
        Projectile();
        virtual ~Projectile();

        virtual void init();

        // return whether to play explosion sound
        virtual bool destroy(){return false;}

        void init(sf::Vector2f from, sf::Vector2f to);

        virtual void update();

        virtual void draw(sf::RenderWindow& window, double interpolation);

        void setDirection (sf::Vector2f direction);

    protected:
    private:
        //sf::Vector2f direction_;

        //temporary bullet
        //shared_ptr<sf::CircleShape> shape_;

        Projectile(const Projectile& other);Projectile& operator=(const Projectile& other);
};

#endif // PROJECTILE_H
