#ifndef FIRSTLEVEL_H
#define FIRSTLEVEL_H

#include <Level.h>
#include <SFML/System/Vector2.hpp>

class FirstLevel : public Level
{
    public:
        FirstLevel();
        virtual ~FirstLevel();

        virtual void init();

        virtual bool update();

    protected:
    private:

        sf::Vector2f portal_;

        FirstLevel(const FirstLevel& other);
        FirstLevel& operator=(const FirstLevel& other);
};

#endif // FIRSTLEVEL_H
