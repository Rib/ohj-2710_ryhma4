#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <tr1/memory>

#include <Ship.h>
#include <ObjectFactory.h>

using std::tr1::shared_ptr;

class Graphics
{
    public:
        Graphics();
        virtual ~Graphics();
        //void init(shared_ptr<ObjectFactory> object_factory);
        static Graphics* inst();
        void init();
        void cleanup();
        void draw(double interpolation);
        shared_ptr<sf::RenderWindow> getWindow() { return window_; }

        void addText(shared_ptr<sf::Text> text)
        {
            texts_.push_back(text);
        }

        double getInterpolation(){return interpolation_;}

    protected:
    private:
        static Graphics* instance_;

        //transforms the window to its correct place
        void windowTransform();

        sf::Texture& getCrosshairTexture() { return crosshair_texture_; }
        void setCrosshairTexture(sf::Texture& crosshair_texture) { crosshair_texture_ = crosshair_texture; }


        shared_ptr<sf::Sprite> crosshair_;
        sf::Texture crosshair_texture_;
        shared_ptr<sf::View> window_view_;
        shared_ptr<sf::RenderWindow> window_;
        shared_ptr<sf::CircleShape> shape_;

        std::vector<shared_ptr<sf::Text> > texts_;

        sf::Texture texture_;

        double interpolation_;
};

#endif // GRAPHICS_H
