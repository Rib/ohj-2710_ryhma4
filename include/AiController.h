#ifndef AICONTROLLER_H
#define AICONTROLLER_H

#include <Controller.h>
#include <Ship.h>


class PhysicalObject;

class AiController : public Controller
{
    public:
        AiController(bool boss);
        virtual ~AiController();

        void control();

        void setTarget(std::tr1::shared_ptr<PhysicalObject> target){target_ = target;}
    protected:
    private:


    void movement();

    void shooting();

    bool boss_;

    bool isShooting_;

    // whether enenmy is going up or down
    Direction dir_;

    bool playerDetected_;

    float targetDistance_;

    // how much we are going to move
    int amount_;

    shared_ptr<PhysicalObject> target_;

    int range_;

        AiController(const AiController& other);
        AiController& operator=(const AiController& other);
};

#endif // AICONTROLLER_H
