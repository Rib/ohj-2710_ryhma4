#ifndef DRAWABLEOBJECT_H
#define DRAWABLEOBJECT_H

#include <iostream>

#include <tr1/memory>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

using std::tr1::shared_ptr;

class DrawableObject
{
    public:
        DrawableObject();
        virtual ~DrawableObject();

        virtual void draw(sf::RenderWindow& window, double interpolation);

        sf::Vector2f getPosition() { return position_; }
        void setPosition(sf::Vector2f position) { position_ = position;}

        const sf::Texture& getTexture() { return texture_; }

        virtual void loadTexture(const std::string& filename, float scale);

        void setId (unsigned int id) { id_ = id; }
        unsigned int getId() { return id_; }

        void setAABB(sf::IntRect AABB) { AABB_ = AABB; }
        void setAABBPosition(float x, float y) { AABB_.left = x; AABB_.top = y;}
        sf::IntRect getAABB () { return AABB_; }

        unsigned int getWidth() {
            return getObjectSprite()->getTextureRect().width *
            getObjectSprite()->getScale().x;
        }

        unsigned int getHeight() {
            return getObjectSprite()->getTextureRect().height *
            getObjectSprite()->getScale().y; }



    protected:

        //void setTexture(sf::Texture texture) { texture_ = texture; }

        void setObjectSprite(shared_ptr<sf::Sprite> sprite) { sprite_ = sprite; }
        shared_ptr<sf::Sprite> getObjectSprite() { return sprite_; }


    private:
        DrawableObject(const DrawableObject& other);
        DrawableObject& operator=(const DrawableObject& other);

        unsigned int id_;

        //rectangle for quick intersection tests
        sf::IntRect AABB_;

        //texture of this object
        sf::Texture texture_;
        //sprite of this object
        shared_ptr<sf::Sprite> sprite_;

        sf::Vector2f position_;
};

#endif // DRAWABLEOBJECT_H
