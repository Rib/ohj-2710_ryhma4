#ifndef STATICOBJECT_H
#define STATICOBJECT_H

#include "PhysicalObject.h"

class StaticObject: public PhysicalObject
{
    public:
        StaticObject();
        virtual ~StaticObject();

        virtual bool destroy();
    protected:
    private:
};

#endif // STATICOBJECT_H
