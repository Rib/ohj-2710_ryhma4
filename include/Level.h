#ifndef LEVEL_H
#define LEVEL_H

#include <tr1/memory>

class Weapon;
class Ship;

class Level
{
    public:
        Level();
        virtual ~Level();

        virtual void init();
        void cleanup();

        virtual bool update();

        //bool intersect (shared_ptr<PhysicalObject>& object);


    protected:
    private:

    unsigned int bossID_;
    bool won_;
    unsigned int victoryTimer_;

    Level(const Level& other);
    Level& operator=(const Level& other);
};

#endif // LEVEL_H
