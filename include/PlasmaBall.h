#ifndef PLASMABALL_H
#define PLASMABALL_H

#include <MovingObject.h>


class PlasmaBall : public MovingObject
{
    public:
        PlasmaBall();
        virtual ~PlasmaBall();

        virtual bool destroy(){return false;}

        void init(sf::Vector2f from, sf::Vector2f to);

        virtual void update();

        virtual void draw(sf::RenderWindow& window, double interpolation);

        void setDirection (sf::Vector2f direction);

    protected:
    private:

    unsigned int timeLeft_;
    PlasmaBall(const PlasmaBall& other);
    PlasmaBall& operator=(const PlasmaBall& other);
};

#endif // PLASMABALL_H
