#ifndef LASER_H
#define LASER_H

#include <cmath>

#include <MovingObject.h>


class Laser : public MovingObject
{
    public:
        Laser();
        virtual ~Laser();

        virtual bool destroy(){return false;}

        void init (sf::Vector2f from, sf::Vector2f to);
        void update();
        void draw(sf::RenderWindow& window, double interpolation);

    protected:
    private:



        Laser(const Laser& other);
        Laser& operator=(const Laser& other);
};

#endif // LASER_H
