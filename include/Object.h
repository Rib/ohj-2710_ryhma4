#ifndef OBJECT_H
#define OBJECT_H

#include <tr1/memory>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

using std::tr1::shared_ptr;

class Object
{
    public:
        Object();
        virtual ~Object();

        virtual void init(); //= 0;

        virtual void update(); // = 0;

        virtual void draw(sf::RenderWindow& window);
        virtual void drawHealthBar();

        sf::Vector2f getPosition() { return position_; }
        void setPosition(sf::Vector2f position)
        { position_ = position;}
   //     getObjectSprite()->setPosition(position_); }

        //checks if object intersect
        bool intersect (shared_ptr<Object>& object);

        unsigned int getDamage () { return damage_; }
        unsigned int getMass() { return mass_; }

        void setDamage( unsigned int damage ) { damage_ = damage; }
        void setMass ( unsigned int mass ) { mass_ = mass; }

        void doDamage( unsigned int damage) { damage_ = damage_ >= damage ? damage_-damage : 0; }

        unsigned int getWidth() { return sprite_->getTextureRect().width*sprite_->getScale().x; }
        unsigned int getHeight() { return sprite_->getTextureRect().height*sprite_->getScale().y; }

        const sf::Texture& getTexture() { return texture_; }

        void loadTexture(const std::string& filename);

        void setAABB(sf::IntRect AABB) { AABB_ = AABB; }
        void setAABBPosition(float x, float y) { AABB_.left = x; AABB_.top = y;}
        sf::IntRect getAABB () { return AABB_; }

    protected:


        //void setTexture(sf::Texture texture) { texture_ = texture; }

        void setObjectSprite(shared_ptr<sf::Sprite> sprite) { sprite_ = sprite; }
        shared_ptr<sf::Sprite> getObjectSprite() { return sprite_; }

    private:

    //position of the object
    sf::Vector2f position_;

    //rectangle for quick intersection tests
    sf::IntRect AABB_;

    //damage of the object
    unsigned int damage_;
    //mass of the object
    unsigned int mass_;
    //texture of this object
    sf::Texture texture_;
    //sprite of this object
    shared_ptr<sf::Sprite> sprite_;

    Object(const Object& other);
    Object& operator=(const Object& other);

};

#endif // OBJECT_H
