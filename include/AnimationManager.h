#ifndef ANIMATIONMANAGER_H
#define ANIMATIONMANAGER_H


#include <string>
#include <list>
#include <SFML/System/Vector2.hpp>
#include <tr1/memory>

class Animation;


class AnimationManager
{
    public:
        AnimationManager();
        ~AnimationManager();

        static AnimationManager* inst();


        void playAnimation(std::string file, sf::Vector2f position, unsigned int images,
                           unsigned int frameTime = 5 );

        void update();

    private:

    static AnimationManager* instance_;
    std::list<std::tr1::shared_ptr<Animation> > animations_;

    AnimationManager(const AnimationManager& other);
    AnimationManager& operator=(const AnimationManager& other);
};

#endif // ANIMATIONMANAGER_H
