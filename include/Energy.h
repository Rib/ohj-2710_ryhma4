#ifndef ENERGY_H
#define ENERGY_H

#include <Subsystem.h>


class Energy : public Subsystem
{
    public:
        Energy();
        virtual ~Energy();
        void update();
    protected:
    private:

        Energy(const Energy& other);
        Energy& operator=(const Energy& other);
};

#endif // ENERGY_H
