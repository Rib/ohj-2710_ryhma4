
//#include <SFML/Graphics.hpp>
//#include <SFML/Window.hpp>
//#include <iostream>

#include "Game.h"


unsigned int const TICKSPERSECOND = 60;
unsigned int const SECOND = 1000;
unsigned int const SKIPTICKS = SECOND/TICKSPERSECOND;
unsigned int MAXFRAMESKIP = 5;


int main()
{
    sf::Clock clock;

    Game game;
    game.init(time(NULL));
    double interpolation = 0;

    long int nextTick = clock.getElapsedTime().asMilliseconds() + SKIPTICKS;
    unsigned int loops = 0;

    // Start the game loop
    while(game.stillGoing())
    {
        loops = 0;

        // update game world 60 times a second
        while(clock.getElapsedTime().asMilliseconds() > nextTick && loops < MAXFRAMESKIP)
        {
            game.update();
            nextTick += SKIPTICKS;

            loops++;
        }

        interpolation = clock.getElapsedTime().asMilliseconds() + SKIPTICKS - nextTick;
        interpolation /= SKIPTICKS;

        // draw the world as often as possible
        game.draw(interpolation);
    }
    game.cleanup();

    return 0;
}
