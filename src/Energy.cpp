#include "Energy.h"

#include "Config.h"

Energy::Energy()
{
    //ctor
}

Energy::~Energy()
{
    //dtor
}

void Energy::update()
{
    //repair();
    if (std::rand()%ENERGY_REGENERATION_RATE == 0) repair();
    Subsystem::update();
}
