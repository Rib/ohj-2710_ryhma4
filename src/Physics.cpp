#include "Physics.h"
#include "ObjectFactory.h"


#include "AnimationManager.h"
#include "SoundManager.h"
#include "Config.h"

#include <iostream>



Physics::Physics() {
    //:objects_()
}
Physics::~Physics()
{
    //dtor
}

//initializations
void Physics::init()
{
    explosionID_ = SoundManager::inst()->loadSound("sounds/explosion.ogg");
    collisionID_ = SoundManager::inst()->loadSound("sounds/collision.ogg");
}

void Physics::checkIntersections()
{
    for(std::map<unsigned int, shared_ptr<PhysicalObject> >::iterator it =
                ObjectFactory::inst()->getMovingObjects()->begin();
            it != ObjectFactory::inst()->getMovingObjects()->end(); ++it)
    {
        //first checking if object has collided with level borders
        //TODO level!
        sf::Vector2f pos = it->second->getPosition();

        if (pos.x < 0 || pos.y < 0 || pos.x > AREA.x) {

            ObjectFactory::inst()->destroyObject(it->second);
            break;
        }

        if (pos.y > AREA.y-(210+128)*Graphics::inst()->getWindow()->getSize().y/1080) {
            AnimationManager::inst()->playAnimation("textures/explosion.png",
                                                    it->second->getPosition(),
                                                    5, 10);
            ObjectFactory::inst()->destroyObject(it->second);
            SoundManager::inst()->playSound(explosionID_);
            break;
        }
    }

    for (std::map<unsigned int, shared_ptr<PhysicalObject> >::iterator i =
                ObjectFactory::inst()->getMovingObjects()->begin();
            i != ObjectFactory::inst()->getMovingObjects()->end(); ++i)
    {
        for (std::map<unsigned int, shared_ptr<PhysicalObject> >::iterator j =
                    ObjectFactory::inst()->getMovingObjects()->begin();
                j != ObjectFactory::inst()->getMovingObjects()->end(); ++j)
        {
            if (i != j &&
                    j->second->
                    intersect(i->second))
            {
                //TODO action on intersect
                i->second->doDamage(j->second->getMass());

                SoundManager::inst()->playSound(collisionID_);

                //calculating where object is heading after the intersection
                //
                //TODO get actForce working better to control the ships with forces
                sf::Vector2f act_force = i->second->getDirection();
                act_force *= i->second->getMass()*FRICTION_FAC*i->second->getSpeed()*60;
                j->second->actForce(act_force);
                //ObjectFactory::inst()->getMovingObjects()->at(i)->doDamage(
                //ObjectFactory::inst()->getMovingObjects()->at(j)->getMass());                                                           )
                std::cout << "physics.cpp:21 Intersect happened\n";
                std::cout << "physics.cpp:21" << " act_force(" << act_force.x << "," << act_force.y << ")\n";
            }
        }
    }
}

void Physics::checkDamage()
{
    for(std::map<unsigned int, shared_ptr<PhysicalObject> >::iterator it =
                ObjectFactory::inst()->getMovingObjects()->begin();
            it != ObjectFactory::inst()->getMovingObjects()->end(); ++it)
    {
        if (it->second->getDamage() == 0 || it->second->getDistanceLeft() == 0)
        {
            if(it->second->destroy())
            {
                SoundManager::inst()->playSound(explosionID_);
            }

            //destroying the object
            ObjectFactory::inst()->destroyObject(it->second);

            break;

            //decrementing the counter because we have one less objects
            it = ObjectFactory::inst()->getMovingObjects()->begin();
            std::cout << "physics.cpp Object destroyed\n";
        }
    }
}

void Physics::moveObjects()
{
    //running through all the objects and updating them locally
    if (!ObjectFactory::inst()->getMovingObjects()->empty())
    {
        for(std::map<unsigned int, shared_ptr<PhysicalObject> >::iterator it =
                    ObjectFactory::inst()->getMovingObjects()->begin();
                it != ObjectFactory::inst()->getMovingObjects()->end(); ++it)
        {
            it->second->update();
        }
    }

    //std::cout << "Physics moveObjects: " << "amount of objects "
    //<< ObjectFactory::inst()->getAllObjects()->size() << "\n";
}
