#include "Laser.h"

#include <Config.h>

Laser::Laser()
{
    //ctor
}

Laser::~Laser()
{
    //dtor
}

void Laser::init(sf::Vector2f from, sf::Vector2f to) {
    //my texture dissappeared somewhere...
    //loadTexture("texture/projectile_1.png");
    loadTexture("textures/laser2.png", 0.35);
    //setting objects aabb
    sf::IntRect aabb(getPosition().x, getPosition().y, getWidth(), getHeight());
    setAABB(aabb);

    setPosition(from);

    //calculating direction for later use
    Movement mov;
    mov.direction = to-from;

    mov.speed = std::sqrt(mov.direction.x*mov.direction.x + mov.direction.y*mov.direction.y);
    mov.direction.x = (mov.direction.x) / mov.speed;
    mov.direction.y = (mov.direction.y) / mov.speed;

    mov.speed = LASERSPEED;

    getObjectSprite()->setOrigin(getTexture().getSize().x/2,getTexture().getSize().y/2);
    setMovement(mov);
    //setting projectiles damage
    setDamage(1);
    //setting how much damage projectile makes
    setMass(LASER_DAMAGE);

    setMaxSpeed(LASERSPEED);

    setDistanceLeft(LASER_DISTANCE);
}

void Laser::update()
{
    // calling this makes the laser ignore air resistance
    sf::Vector2f force(0, 0);
    actForce(force);
    MovingObject::update();

    reduceDistance();

    getObjectSprite()->setRotation(atan2(getDirection().y, getDirection().x)*180/PI + 180);
}

void Laser::draw(sf::RenderWindow& window, double interpolation)
{
    //window.draw(*shape_);

    DrawableObject::draw(window, interpolation);
}
