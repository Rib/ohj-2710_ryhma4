#include "../include/MovingObject.h"

//#include "PhysicalObject.h"

//#include "Config.h"

#include <math.h>


MovingObject::MovingObject()
    :
//    velocity_(),
    moving_(),
    airResistance_(0.0),
    maxSpeed_(5),
    force_(false),
    gravity_(0),
    accelerationStep_(),
    steps_(60)
{
    moving_.speed = 0;
    moving_.direction.x = 0;
    moving_.direction.y = 0;
    accelerationStep_.x = 0;
    accelerationStep_.y = 0;
    //ctor
}

MovingObject::~MovingObject()
{
    //dtor
}

void MovingObject::collide(sf::Vector2f velocity, unsigned int mass)
{
    sf::Vector2f newVecocity(moving_.direction.x*moving_.speed*(getMass() - mass)
        + 2*mass*velocity.x/(getMass()+mass)*velocity.x,
                             moving_.direction.y*moving_.speed*(getMass() - mass)
        + 2*mass*velocity.y/(getMass()+mass)*velocity.y);
    moving_.speed = sqrt(newVecocity.x*newVecocity.x + newVecocity.y*newVecocity.y);

    if(moving_.speed  != 0)
    {
        moving_.direction.x = newVecocity.x/moving_.speed;
        moving_.direction.y = newVecocity.y/moving_.speed;
    }

    std::cout << "speed after collision: " << moving_.speed << std::endl;
    std::cout << "dir after collision: " << moving_.direction.x << ", "
                                         << moving_.direction.y  << std::endl;

    force_ = true;
}

void MovingObject::actForce(sf::Vector2f force)
{
    // a = F/m per steps in second
    accelerationStep_.x += (force.x/getMass());
    accelerationStep_.y += (force.y/getMass());
    force_ = true;

}


void MovingObject::update()
{
    // effect of gravity
    accelerationStep_.y += gravity_;

    sf::Vector2f oldMovement(moving_.direction.x*moving_.speed,
                             moving_.direction.y*moving_.speed);

    oldMovement += accelerationStep_;

    moving_.speed = sqrt(oldMovement.x*oldMovement.x + oldMovement.y*oldMovement.y);

    if(moving_.speed  != 0)
    {
        moving_.direction.x = oldMovement.x/moving_.speed;
        moving_.direction.y = oldMovement.y/moving_.speed;
    }

    if(moving_.speed > maxSpeed_)
    {
        moving_.speed = maxSpeed_;
    }

    if(!force_)
    {
        moving_.speed *= 0.9;
    }

    setPosition(getPosition() + sf::Vector2f(moving_.direction.x*moving_.speed,moving_.direction.y*moving_.speed));


    //setPosition(getPosition() + oldMovement);

    accelerationStep_.x = 0;
    accelerationStep_.y = 0;

    force_ = false;

    //setting aabb position
    setAABBPosition(getPosition().x, getPosition().y);
}

void MovingObject::draw(sf::RenderWindow& window, double interpolation)
{
    // float type is too impresise i think. You can draw both to see how it differs.
    //getObjectSprite()->setPosition(getPosition()+ sf::Vector2f(moving_.direction.x*moving_.speed*interpolation,
    //                                                            moving_.direction.y*moving_.speed*interpolation));
    //window.draw(*getObjectSprite());

    DrawableObject::draw(window, interpolation);
}

