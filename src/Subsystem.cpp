#include <Subsystem.h>

const unsigned int MAXHEALTH = 100;

Subsystem::Subsystem():
health_(MAXHEALTH),
max_health_(MAXHEALTH),
repairing_(false)
{
    //ctor
}

Subsystem::~Subsystem()
{
    //dtor
}

void Subsystem::doDamage(unsigned int damage) {
    if (damage == 0) return;

    if (health_ < damage) health_ = 0;
    else health_ -= damage;
}



void Subsystem::update()
{
    if(repairing_ && health_ < max_health_)
    {
        ++health_;
    }
    repairing_ = false;
}
