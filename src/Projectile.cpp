#include <Projectile.h>

#include <Config.h>

Projectile::Projectile()
{
    //ctor
}

Projectile::~Projectile()
{
    //dtor
}

void Projectile::init() {
}

void Projectile::init(sf::Vector2f from, sf::Vector2f to) {
    //my texture dissappeared somewhere...
    //loadTexture("texture/projectile_1.png");
    loadTexture("textures/plasmaball.png", 0.15);
    //setting objects aabb
    sf::IntRect aabb(getPosition().x, getPosition().y, getWidth(), getHeight());
    setAABB(aabb);

    setPosition(from);
    //calculating direction for later use
    std::cout << "Projectile init: " << "to->(" << to.x << ", " << to.y << ")\n";
     std::cout << "Projectile init: " << "from->(" << from.x << ", " << from.y << ")\n";

    Movement mov;
    mov.direction = to-from;

    std::cout << "Projectile init: " << "direction->(" << mov.direction.x << ", " << mov.direction.y << ")\n";
    mov.speed = std::sqrt(mov.direction.x*mov.direction.x + mov.direction.y*mov.direction.y);
    mov.direction.x = (mov.direction.x) / mov.speed;
    mov.direction.y = (mov.direction.y) / mov.speed;

    mov.speed = PROJECTILESPEED;
    std::cout << "Projectile init: " << "direction->(" << mov.direction.x << ", " << mov.direction.y << ")\n";

    getObjectSprite()->setOrigin(getTexture().getSize().x/2,getTexture().getSize().y/2);
    setMovement(mov);
    //setting projectiles damage
    setDamage(1);
    //setting how much damage projectile makes
    setMass(PROJECTILE_DAMAGE);

    setMaxSpeed(PROJECTILESPEED);

    setDistanceLeft(PROJECTILE_DISTANCE);
}

/*void Projectile::update() {
    getObjectSprite()->move(direction_.x*getMovementSpeed(),direction_.y*getMovementSpeed());
    setPosition(getObjectSprite()->getPosition());
}*/

void Projectile::update()
{
    // calling this makes the projectile ignore air resistance
    sf::Vector2f force(0, 0);
    actForce(force);
    MovingObject::update();
    getObjectSprite()->rotate(21);

    reduceDistance();
}

void Projectile::draw(sf::RenderWindow& window, double interpolation)
{
    //window.draw(*shape_);

    DrawableObject::draw(window, interpolation);
}
