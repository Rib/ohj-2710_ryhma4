#include "Object.h"


#include <iostream>
Object::Object()
:
damage_(100),
mass_(1)
{
    //ctor
}

Object::~Object()
{
    //dtor
}

void Object::init()
{

}

void Object::update()
{

}

bool Object::intersect (shared_ptr<Object>& object) {
    // TODO implementation
    return getAABB().intersects(object->getAABB());
}

void Object::draw(sf::RenderWindow& window)
{
    sprite_->setPosition(position_);
    window.draw(*sprite_);
}

void Object::drawHealthBar(){}

void Object::loadTexture(const std::string& filename)
{
    texture_.loadFromFile(filename);

    std::cout << "Load texture: " << " creating sprite\n";
    shared_ptr<sf::Sprite> sprite(new sf::Sprite(getTexture()));
    //TODO calculate scale from the window
    sprite->setScale(0.5,0.5);

    //sets the initial position of the ship
    std::cout << "Load texture: " << " setting ship position\n";
    //position vector
    //sf::Vector2f position;
    //position_.x = 200;
    //position_.y = 200;

    //sets objects position on the window
    sprite->setPosition(position_);
    //setPosition(position);
    //setting object sprite
    setObjectSprite(sprite);

    //setting objects aabb
    sf::IntRect aabb(getPosition().x, getPosition().y, getWidth(), getHeight());
    setAABB(aabb);
}
