#include "Animation.h"

Animation::Animation(unsigned int images, unsigned int frameTime)
    :
    frames_(images),
    currentFrame_(0),
    frameTime_(frameTime),
    counter_(frameTime)

{
    position_.x = 0;
    position_.y = 0;
    //ctor
}

Animation::~Animation()
{
    //dtor
}

void Animation::loadTexture(const std::string& filename)
{

    DrawableObject::loadTexture(filename, 0.5);
    size_.y = getTexture().getSize().y;
    size_.x = getTexture().getSize().x/frames_;
    getObjectSprite()->setTextureRect(sf::IntRect(position_, size_));
}

bool Animation::animationUpdate()
{
    if(counter_ == 0)
    {
        ++currentFrame_;
        if(currentFrame_ == frames_)
        {
            return false;
        }
        counter_ = frameTime_;
        position_.x += getTexture().getSize().x/frames_;
        getObjectSprite()->setTextureRect(sf::IntRect(position_, size_));
    }
    --counter_;
    return true;
}
