#include <Weapon.h>


#include <Config.h>

#include "SoundManager.h"

Weapon::Weapon(bool left)
    :
    shoot_(false),
    clipSize_(1), // are set again in init
    clipLeft_(1),
    shotDelay_(5),
    delayLeft_(0),
    loading_time_(60),
    loading_time_remaining_(0),
    wasShooting_(false),
    lastDir_(false),
    left_(left)
{
    //ctor
}

Weapon::~Weapon()
{
    //dtor
}

void Weapon::init(shared_ptr<Ship> ship, WeaponType type,
                  unsigned int clipSize,
                  unsigned int shotDelay,
                  unsigned int loadingTime,
                  unsigned int sound)
{
    clipSize_ = clipSize;
    clipLeft_ = clipSize_;

    sound_ = sound;
    shotDelay_ = shotDelay;
    //object_factory_ = object_factory;
    type_ = type;
    ship_ = ship;
    loading_time_ = loadingTime;
    lastDir_ = bool(rand()%2);
    stepAngle_ = PI/2/clipSize_;
}

void Weapon::update()
{
    if(loading_time_remaining_ > 0)
    {
        -- loading_time_remaining_;
        if(loading_time_remaining_ == 0)
        {
            clipLeft_ = clipSize_;
        }
    }
    else if(clipLeft_ == 0)
    {
        loading_time_remaining_ = loading_time_;
        wasShooting_ = false;
    }
    else if(shoot_ || (type_ == PLASMA && wasShooting_))
    {
        if(delayLeft_ == 0)
        {
            --clipLeft_;
            shooting();
            delayLeft_ = shotDelay_;
            wasShooting_ = true;
        }
        else
        {
            --delayLeft_;
        }
    }
    shoot_ = false;
    Subsystem::update();
}

void Weapon::shoot(sf::Vector2f target_pos)
{
    // TODO vähennä hiiren sijainnista ikkunan sijainti window->getPosition()
    // TODO siirrä hiiren koodi humancontrolleriin

    target_ = target_pos;
    shoot_ = true;


}

sf::Vector2f Weapon::getShipPosition()
{
    return ship_->getPosition();
}


void Weapon::shooting()
{
    SoundManager::inst()->playSound(sound_);

    switch (getWeaponType())
    {
    case PROJECTILE:
    {
        ObjectFactory::inst()->createProjectile(shootLoc(), target_);
        break;
    }
    case ROCKET:
    {
        ObjectFactory::inst()->createRocket(ship_->getPosition() + sf::Vector2f(ship_->getWidth()/2, ship_->getHeight()+ 50),
                                            ship_->getMovement(), left_);
        break;
    }
    case LASER: {
        ObjectFactory::inst()->createLaser(shootLoc(), target_);
        break;
    }
    case PLASMA:
    {
        sf::Vector2f shootFrom = shootLoc();
        if(!wasShooting_)
        {
            sf::Vector2f shotVector = target_ - shootFrom;

            if(lastDir_)
            {
                currentAngle_ = atan2(shotVector.y, shotVector.x) + PI/4;
            }
            else
            {
                currentAngle_ = atan2(shotVector.y, shotVector.x) - PI/4;
            }
            lastDir_ = !lastDir_;

        }
        else if(lastDir_)
        {
            currentAngle_ += stepAngle_;
        }
        else
        {
            currentAngle_ -= stepAngle_;
        }

        ObjectFactory::inst()->createPlasma(shootFrom, sf::Vector2f(cos(currentAngle_)+shootFrom.x ,
                                                                  sin(currentAngle_)+shootFrom.y ));
        break;
    }
    }
}


sf::Vector2f Weapon::shootLoc()
{
        sf::Vector2f ship_pos = getShipPosition();
        unsigned int ship_width = getShip()->getWidth();
        unsigned int ship_height = getShip()->getHeight();
        //counting difference of position
        //TODO get projectile launching position correct
        int diff_y = 0;

        diff_y = target_.y > ship_pos.y+ship_height || target_.y < ship_pos.y ?
                 ship_height : target_.y-ship_pos.y;
        std::cout << "Weapon shoot: " << " diffy 1: " << diff_y << "\n";
        diff_y = target_.y < ship_pos.y  ? -25 : diff_y;
        std::cout << "Weapon shoot: " << " diffy 2: " << diff_y << "\n";
        int diff_x = 0;
        diff_x = target_.x > ship_pos.x+ship_width || target_.x < ship_pos.x ?
                 ship_width : target_.x-ship_pos.x;
        std::cout << "Weapon shoot: " << " diffx 1: " << diff_x << "\n";
        diff_x = target_.x < ship_pos.x  ? -25 : diff_x;
        std::cout << "Weapon shoot: " << " diffx 2: " << diff_x << "\n";

        //assgning new projectile starting values
        ship_pos.y = ship_pos.y + diff_y;
        ship_pos.x = ship_pos.x + diff_x;
        return ship_pos;
}
