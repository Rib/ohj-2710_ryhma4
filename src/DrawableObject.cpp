#include "DrawableObject.h"

#include <Graphics.h>

static unsigned int global_id = 1;

DrawableObject::DrawableObject():id_(global_id++)
{
    //ctor
}

DrawableObject::~DrawableObject()
{
    //dtor
}

void DrawableObject::draw(sf::RenderWindow& window, double interpolation)
{
    getObjectSprite()->setPosition(getPosition());
    window.draw(*getObjectSprite());
}

void DrawableObject::loadTexture(const std::string& filename, float scale)
{
    texture_.loadFromFile(filename);

    //std::cout << "Load texture: " << " creating sprite\n";
    shared_ptr<sf::Sprite> sprite(new sf::Sprite(getTexture()));
    //TODO calculate scale from the window
    sprite->setScale(scale*Graphics::inst()->getWindow()->getSize().x/1920,scale*Graphics::inst()->getWindow()->getSize().y/1080);

    //sets the initial position of the ship
    //std::cout << "Load texture: " << " setting position\n";
    //position vector
    //sf::Vector2f position;
    //position_.x = 200;
    //position_.y = 200;

    //sets objects position on the window
    sprite->setPosition(position_);
    //setPosition(position);
    //setting object sprite
    setObjectSprite(sprite);

    //setting objects aabb
    sf::IntRect aabb(getPosition().x, getPosition().y, getWidth(), getHeight());
    setAABB(aabb);
}
