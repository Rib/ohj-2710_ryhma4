#include "Rocket.h"


#include "Config.h"

#include "AnimationManager.h"

Rocket::Rocket()
    :
    smoke_(10),
    smokeLeft_(0)
{
    //ctor
}

Rocket::~Rocket()
{
    //dtor
}


void Rocket::init(bool left)
{
    left = left;

    if(left)
    {
        force_.x =-50;
    }
    else
    {
        force_.x =50;
    }
    force_.y = 0;
    loadTexture("textures/rocket.png", 0.25);

    sf::IntRect aabb(getPosition().x, getPosition().y, getWidth(), getHeight());
    setAABB(aabb);

    setDamage(1);
    sf::Vector2f force(0, 2);
    actForce(force);

    setMaxSpeed(PROJECTILESPEED*5);
    setMass(ROCKET_DAMAGE);

    setDistanceLeft(ROCKET_DISTANCE);
}

void Rocket::update()
{
    actForce(force_);
    MovingObject::update();

    if(smokeLeft_ == 0)
    {
        if(left_)
        {
            AnimationManager::inst()->playAnimation("textures/smoke1.png",
                                                    getPosition() + sf::Vector2f(20+ getWidth(), 10),
                                                    5, 10);
        }
        else
        {
            AnimationManager::inst()->playAnimation("textures/smoke1.png",
                                                    getPosition()- sf::Vector2f(20, 10),
                                                    5, 10);
        }


        smokeLeft_ = smoke_ + rand()%(smoke_/2);
    }
    --smokeLeft_;

    reduceDistance();
}

void Rocket::draw(sf::RenderWindow& window, double interpolation)
{
    DrawableObject::draw(window, interpolation);
}
