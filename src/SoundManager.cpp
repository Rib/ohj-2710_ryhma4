#include "SoundManager.h"


const unsigned int SOUNDLIMIT = 64;

SoundManager* SoundManager::instance_ = 0;

SoundManager::SoundManager()
{
    //ctor
}

SoundManager::~SoundManager()
{
    for(unsigned int i = 0; i < sounds_.size(); ++i)
    {
        delete sounds_.at(i);
    }

    for(unsigned int i = 0; i < buffers_.size(); ++i)
    {
        delete buffers_.at(i);
    }


    //dtor
}

SoundManager* SoundManager::inst()
{
    if(instance_ == 0)
    {
        instance_ = new SoundManager();
    }

    return instance_;
}

unsigned int SoundManager::loadSound(std::string file)
{
    sf::SoundBuffer* s = new sf::SoundBuffer;
    s->loadFromFile(file);
    buffers_.push_back(s);
    return buffers_.size() - 1;
}

void SoundManager::playSound(unsigned int id)
{
    if(sounds_.size() > SOUNDLIMIT)
    {
        for(unsigned int i = 0; i < sounds_.size(); ++i)
        {
            if(sounds_.at(i)->getStatus() == sf::SoundSource::Stopped)
            {
                sounds_.at(i)->setBuffer(*buffers_.at(id));
                sounds_.at(i)->play();
                break;
            }
        }
    }
    else
    {
    sf::Sound* sound = new sf::Sound;
    sound->setBuffer(*buffers_.at(id));
    sound->play();
    sounds_.push_back(sound);
    }
}

void SoundManager::playMusic(std::string file)
{
    music_.openFromFile(file);
    music_.play();
    music_.setVolume(50);
    music_.setLoop(true);
}
