#include "Shield.h"

#include "Ship.h"

Shield::Shield():
    disabled_(false)
{
    //ctor
}

Shield::~Shield()
{
    //dtor
}

void Shield::update()
{

    if (!disabled_ && std::rand()%SHIELD_REGENERATION_RATE == 0 && getHealth() < getMaxHealth() &&

         getShip()->getSubSystems().at(ENERGY)->getHealth() > 0) {
        getShip()->getSubSystems().at(ENERGY)->doDamage(4);
        repair();
    }
    Subsystem::update();
}

void Shield::doDamage(unsigned int damage) {
    Subsystem::doDamage(damage);
    /*if (damage == 0) return;

    unsigned int max_heal = getHealth();

    if (disabled_ && getHealth() < damage) assignHealth(0);
    else if (disabled_) assignHealth(getHealth() - damage);
    else if (!disabled_ && max_heal > 0 && damage <= max_heal) {
        getShip()->getSubSystems().at(SHIELD)->doDamage(damage);
    } else {
        damage -= max_heal;
        getShip()->getSubSystems().at(SHIELD)->doDamage(max_heal);
        if (getHealth() < damage) assignHealth(0);
        else assignHealth(getHealth() - damage);
    }*/
}
