#include "Hull.h"

#include "Config.h"

Hull::Hull()
{
    //ctor
}

Hull::~Hull()
{
    //dtor
}

void Hull::update()
{
    getShip()->setEnginePower(getHealth()*getHealth()*ENGINEPOWER/getMaxHealth()/getMaxHealth());

    if (std::rand()%HULL_REGENERATION_RATE == 0) repair();
    Subsystem::update();
}
