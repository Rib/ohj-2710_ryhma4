#include "AiController.h"

#include "SoundManager.h"
#include "Config.h"

const Direction DUPRIGHT [] = {NONE, UP, UPRIGHT, RIGHT};

const Direction DDOWNRIGHT [] = {NONE, RIGHT, DOWNRIGHT, DOWN};

//const Direction DDOWNLEFT [] = {NONE, DOWN, DOWNLEFT, LEFT};
const Direction DDOWNLEFT [] = {NONE, DOWN, DOWNLEFT, DOWNRIGHT};

//const Direction DUPLEFT [] = {NONE, LEFT, UPLEFT, UP};
const Direction DUPLEFT [] = {NONE, UPLEFT, UP, UPRIGHT};


const unsigned int DIRSIZE  = 4;


AiController::AiController(bool boss):
boss_(boss),
    dir_(NONE),
    playerDetected_(false),
    amount_(0),
    range_(AIRANGE)
{
    //ctor
}

AiController::~AiController()
{
    //dtor
}

void AiController::control()
{
    shooting();
    movement();

}


void AiController::movement()
{
    if(playerDetected_ && amount_ <= 0)
    {


        if(target_->getPosition().x < getShip()->getPosition().x)
        {
            if(target_->getPosition().y < getShip()->getPosition().y)
            {

                dir_ = DUPLEFT[rand()%DIRSIZE];
            }
            else
            {
                // mikäli ei olla lähellä maan pintaa
                if(getShip()->getPosition().y < AREA.y - AIGROUNDAVOIDANCE)
                {
                    dir_ = DDOWNLEFT[rand()%DIRSIZE];
                }

            }
        }
        else if(target_->getPosition().y < getShip()->getPosition().y)
        {
            dir_ = DUPRIGHT[rand()%DIRSIZE];
        }
        else
        {

            dir_ = DDOWNRIGHT[rand()%DIRSIZE];
        }


        amount_ = rand()%60;
    }
    // if ai is too close to the ground
    if(getShip()->getPosition().y > AREA.y - AIGROUNDAVOIDANCE - getShip()->getHeight())
    {
        getShip()->move(UP);
    }
    else if(( targetDistance_ > 400 && (getShip()->getWeapons().at(getShip()->getCurrentWeapon())->getWeaponType() != PLASMA || !getShip()->getWeapons().at(getShip()->getCurrentWeapon())->isShooting())
                                         && (getShip()->getWeapons().at(getShip()->getCurrentWeapon())->getWeaponType() != ROCKET || !isShooting_)))
    {
        getShip()->move(dir_);
    }
    --amount_;
}


void AiController::shooting()
{
    isShooting_ = false;

    sf::Vector2f targetPos = target_->getPosition() +
                             sf::Vector2f(rand()%(2*ENEMYACCURACY+1) - ENEMYACCURACY,
                                          rand()%(2*ENEMYACCURACY+1) - ENEMYACCURACY);

    sf::Vector2f targetVector =  targetPos - getShip()->getPosition();

    targetDistance_ = sqrt(targetVector.x*targetVector.x + targetVector.y*targetVector.y);

    // estimate position from current speed if not too far for it to make sense
    if(targetDistance_ < 1200)
        targetPos += targetDistance_/PROJECTILESPEED*target_->getSpeed()*target_->getDirection();

    if(targetDistance_ < range_)
    {
        if(boss_)
        {
            SoundManager::inst()->playMusic("sounds/bossfight.wav");
            boss_ = false;
        }
        playerDetected_ = true;
        if(getShip()->getWeapons().at(getShip()->getCurrentWeapon())->getWeaponType() == PROJECTILE)
        {
            isShooting_ = true;
        }
        else if(getShip()->getWeapons().at(getShip()->getCurrentWeapon())->getWeaponType() == ROCKET)
        {
            if(targetPos.x < getShip()->getPosition().x
                && targetPos.y < getShip()->getPosition().y + 2*ENEMYACCURACY
                && targetPos.y > getShip()->getPosition().y - 2*ENEMYACCURACY)
            {
                isShooting_ = true;
            }
        }
        else if(getShip()->getWeapons().at(getShip()->getCurrentWeapon())->getWeaponType() == PLASMA)
        {
            isShooting_ = true;
        }

    }
    getShip()->shoot(isShooting_, targetPos);

}
