#include "Game.h"

#include "HumanController.h"
#include "AnimationManager.h"
#include "SoundManager.h"

Game::Game()
    :
    physics_()
{
    //ctor
}

Game::~Game()
{
    //dtor
}

void Game::init(long int seed)
{

    std::cout << "Staring initialization" << std::endl;
    ObjectFactory::inst()->init();

    std::cout << "Iniating Physics" << std::endl;
    //initalizing physics and graphics
    physics_.init();

    std::cout << "Iniating Graphics" << std::endl;
    //graphics_.init(ObjectFactory::inst());
    Graphics::inst()->init();

    //std::cout << "Creating player" << std::endl;
    //ObjectFactory::inst()->createHuman();

    srand(seed);

    //ObjectFactory::inst()->createHuman();
    //ObjectFactory::inst()->createLevel();
    ObjectFactory::inst()->createGame();

    //ObjectFactory::inst()->createEnemy();

    std::cout << "Initialization complete" << std::endl;
}

void Game::cleanup()
{
    ObjectFactory::inst()->cleanup();
    Graphics::inst()->cleanup();
}

bool Game::stillGoing()
{
    return Graphics::inst()->getWindow()->isOpen();
}

void Game::update()
{
    sf::Event event;
    while (Graphics::inst()->getWindow()->pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
        {
            Graphics::inst()->getWindow()->close();
        }
    }

    //updating opjects in the factory
    ObjectFactory::inst()->update();
    AnimationManager::inst()->update();

    //reading player controls
    //ObjectFactory::inst()->getPlayer()->control();
    // reading both players and ais controls
    for(unsigned int i = 0; i < ObjectFactory::inst()->getControllers()->size(); ++i)
    {
        ObjectFactory::inst()->getControllers()->at(i)->control();
    }

    //updating physics
    physics_.moveObjects();
    physics_.checkIntersections();
    physics_.checkDamage();



    if(ObjectFactory::inst()->getPlayer()->wantsToQuit())
    {
        Graphics::inst()->getWindow()->close();
    }

    //physics_.checkIntersections();
}

void Game::draw(float interpolation)
{
    //std::cout << "drawing with interpolation: " << interpolation << std::endl;
    Graphics::inst()->draw(interpolation);
}
