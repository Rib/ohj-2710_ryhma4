#include "GravitationalStabilizer.h"

#include <Ship.h>

GravitationalStabilizer::GravitationalStabilizer()
{
    //ctor
}

GravitationalStabilizer::~GravitationalStabilizer()
{
    //dtor
}

void GravitationalStabilizer::update()
{
    getShip()->setGravity(1.-static_cast<float>(getHealth())/static_cast<float>(getMaxHealth()));

    /*if(getHealth() > getMaxHealth()*3/4)
    {
        //TODO return setGravity to 0.0 when some strange bug has been fixed


    }
    else if (getHealth() > getMaxHealth()/2) {
        getShip()->setGravity(.25);
    }
    else if (getHealth() > getMaxHealth()/4) {
        getShip()->setGravity(.5);
    }
    else if (getHealth() > 0) {
        getShip()->setGravity(.75);
    }
    else
    {
        //TODO return setGravity to 1.0 when some strange bug has been fixed
        getShip()->setGravity(1.0);

    }*/
    if (std::rand()%STABILIZER_REGENERATION_RATE == 0) repair();
    Subsystem::update();
}
