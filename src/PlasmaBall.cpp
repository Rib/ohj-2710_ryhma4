#include "PlasmaBall.h"

#include <Config.h>
#include <math.h>
PlasmaBall::PlasmaBall()
:
timeLeft_(120)
{
    //ctor
}

PlasmaBall::~PlasmaBall()
{
    //dtor
}

void PlasmaBall::init(sf::Vector2f from, sf::Vector2f to)
{
    //my texture dissappeared somewhere...
    //loadTexture("texture/projectile_1.png");
    loadTexture("textures/plasmaball2.png", 0.15);
    //setting objects aabb
    sf::IntRect aabb(getPosition().x, getPosition().y, getWidth(), getHeight());
    setAABB(aabb);

    setPosition(from);
    //calculating direction for later use
    std::cout << "PlasmaBall init: " << "to->(" << to.x << ", " << to.y << ")\n";
     std::cout << "PlasmaBall init: " << "from->(" << from.x << ", " << from.y << ")\n";

    Movement mov;
    mov.direction = to-from;

    std::cout << "PlasmaBall init: " << "direction->(" << mov.direction.x << ", " << mov.direction.y << ")\n";
    mov.speed = sqrt(mov.direction.x*mov.direction.x + mov.direction.y*mov.direction.y);
    mov.direction.x = (mov.direction.x) / mov.speed;
    mov.direction.y = (mov.direction.y) / mov.speed;

    mov.speed = PROJECTILESPEED;
    std::cout << "PlasmaBall init: " << "direction->(" << mov.direction.x << ", " << mov.direction.y << ")\n";

    getObjectSprite()->setOrigin(getTexture().getSize().x/2,getTexture().getSize().y/2);
    setMovement(mov);
    //setting projectiles damage
    setDamage(1);

    setMass(PLASMABALL_DAMAGE);

    setMaxSpeed(PROJECTILESPEED/2);
}

/*void Projectile::update() {
    getObjectSprite()->move(direction_.x*getMovementSpeed(),direction_.y*getMovementSpeed());
    setPosition(getObjectSprite()->getPosition());
}*/

void PlasmaBall::update()
{
    // calling this makes the projectile ignore air resistance
    sf::Vector2f force(0, 0);
    actForce(force);
    MovingObject::update();
    getObjectSprite()->setRotation(atan2(getDirection().y, getDirection().x)*180/PI + 180);
}

void PlasmaBall::draw(sf::RenderWindow& window, double interpolation)
{
    //window.draw(*shape_);

    DrawableObject::draw(window, interpolation);
}
