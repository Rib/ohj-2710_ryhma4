#include "Level.h"
#include "DrawableObject.h"
#include "StaticObject.h"
#include "ObjectFactory.h"
#include "AiController.h"
#include "HumanController.h"
#include "SoundManager.h"

#include "Config.h"

#include <iostream>
Level::Level()
:
won_(false)
{
    //ctor
}

Level::~Level()
{
    //dtor
}

void Level::init()
{
    std::cout << "Level init: " << "creating stars" << std::endl;

    SoundManager::inst()->playMusic("sounds/background_music.wav");
    ObjectFactory::inst()->createHuman();

    for (unsigned int i = 0; i < AREA.x*AREA.y/STARDENSITY; ++i)
    {
        shared_ptr<DrawableObject> star(new DrawableObject);
        (*ObjectFactory::inst()->getDrawableObjects())[star->getId()] = star;
        sf::Vector2f start(rand()%AREA.x, rand()%AREA.y);
        star->setPosition(start);
        //std::cout << "Stars init: " << "new star added to" << start.x <<"," << start.y << std::endl;
        star->loadTexture("textures/star.png", 0.5);
    }

    for (unsigned int i = 0; i< CLOUDS; ++i) {
        shared_ptr<DrawableObject> cloud(new DrawableObject);
        (*ObjectFactory::inst()->getDrawableObjects())[cloud->getId()] = cloud;
        sf::Vector2f start(rand()%AREA.x, rand()%AREA.y-400);
        cloud->setPosition(start);
        //std::cout << "Stars init: " << "new star added to" << start.x <<"," << start.y << std::endl;
        cloud->loadTexture("textures/cloud_1.png", 0.5);
    }

    unsigned int xLoc = 0;
    std::vector<std::string> textures;
    textures.push_back("textures/sand1.png");
    textures.push_back("textures/sand2.png");
    textures.push_back("textures/sand3.png");
    textures.push_back("textures/sand4.png");

    std::cout << "Level init: " << "creating ground" << std::endl;
    while (xLoc < AREA.x)
    {
        shared_ptr<StaticObject> ground (ObjectFactory::inst()->createGround());

        unsigned int tIndex = rand()%textures.size();
        ground->loadTexture(textures.at(tIndex), 1);

        ground->setPosition(sf::Vector2f(xLoc, AREA.y - ground->getHeight()));

        xLoc += ground->getWidth();

    }

    xLoc = ENEMYSTART;

    for(unsigned int i = 0;  i < SECOND_LEVEL_ENEMIES; ++i)
    {
        shared_ptr<AiController> enemy = ObjectFactory::inst()->createEnemy("textures/enemyship.png", 0.5);

        enemy->getShip()->setPosition(sf::Vector2f(xLoc, AREA.y/2 + rand()%401 - 200));
        enemy->getShip()->setEnginePower(ENEMYENGINEPOWER);
/*
        for (unsigned int i = 0; i < enemy->getShip()->getSubSystems().size(); ++i) {
            enemy->getShip()->getSubSystems().at(i)->setHealth(enemy->getShip()->getSubSystems().at(i)->getHealth()/4);
        }
*/

        ObjectFactory::inst()->armShip(shared_ptr<Weapon>(new Weapon()), enemy->getShip());
        //enemy->getShip()->setCurrentWeapon(0);

        xLoc += (AREA.x - ENEMYSTART- BOSSZONE)/SECOND_LEVEL_ENEMIES;
    }


    shared_ptr<AiController> enemy = ObjectFactory::inst()->createEnemy("textures/boss1.png", 3.0, true);

    enemy->getShip()->setPosition(sf::Vector2f(AREA.x - enemy->getShip()->getWidth() - 50, AREA.y/2));
    bossID_ = enemy->getShip()->getId();

    ObjectFactory::inst()->armPlasma(shared_ptr<Weapon>(new Weapon()), enemy->getShip());
    //enemy->getShip()->setCurrentWeapon(0);
}


void Level::cleanup()
{
    //props_.clear();

    //ObjectFactory::inst()->getStaticObjects()->clear();
    //ObjectFactory::inst()->getDrawableObjects()->clear();
    //ObjectFactory::inst()->getMovingObjects()->clear();
    //ObjectFactory::inst()->getControllers()->clear();
    ObjectFactory::inst()->init();


}


bool Level::update()
{
    if(ObjectFactory::inst()->getMovingObjects()->find(bossID_) == ObjectFactory::inst()->getMovingObjects()->end() && !won_)
    {
        shared_ptr<sf::Text> victory(new sf::Text("And so the evil *insert empire name here* falls and the multiverse is a safe place once more."));
        victory->setPosition(ObjectFactory::inst()->getPlayer()->getShip()->getPosition());
        Graphics::inst()->addText(victory);

        won_ = true;
        return true;
    }
    return false;
}
