#include "Ship.h"
#include "GravitationalStabilizer.h"
#include "Hull.h"
#include "Armor.h"
#include "Shield.h"
#include "Energy.h"

#include "Config.h"

Ship::Ship():
    moving_(NONE),
    enginePower_(1.0),
    current_weapon_(0)
{
    //ctor
}

Ship::~Ship()
{
    //dtor
}

void Ship::init()
{
    std::cout << "Iniating ship" << std::endl;

    //loads ships texture
    //std::cout << "Ship init: " << " loading textures\n";
    //loadTexture("textures/ship_1.png");
    //creates new sprite from created texture

    //setting movement speed
    enginePower_ = ENGINEPOWER;

    std::cout << "Ship init: " << " initializing weapons\n";
    //initializsing weapon(s)

    //shared_ptr<Ship> ship (this);
    //shared_ptr<Weapon> weapon(new Weapon());
    //weapon->init(ship);
    //weapons_.push_back(weapon);
//    setCurrentWeapon(0);

    //setMovementPower(50);

    //creating hull
    shared_ptr<Hull> hull (new Hull);
    subSystems_.push_back(hull);
    hull->setShip(this);


    //creating armor
    shared_ptr<Armor> armor (new Armor);
    subSystems_.push_back(armor);
    armor->setShip(this);


    //creating shield
    shared_ptr<Shield> shield (new Shield);
    subSystems_.push_back(shield);
    shield->setShip(this);

    //making sheild halo
    shield_halo_texture_.loadFromFile("textures/shield_halo.png");
    shield_halo_.reset(new sf::Sprite(shield_halo_texture_));

    //creating gravitational stabilizer
    shared_ptr<GravitationalStabilizer> grav(new GravitationalStabilizer);
    subSystems_.push_back(grav);
    grav->setShip(this);


    shared_ptr<Energy> energy(new Energy);
    subSystems_.push_back(energy);
    energy->setShip(this);


    setMaxSpeed(SHIPMAXSPEED);
}


void Ship::update()
{
    movement();
    MovingObject::update();
    sf::Vector2f pos = getPosition();
    if (pos.x < 0)
    {
        pos.x = 0;

    }
    else if(pos.x > AREA.x - getWidth())
    {
        pos.x = AREA.x - getWidth();
    }

    if (pos.y < 0) pos.y = 0;
    setPosition(pos);
    shield_halo_->setPosition(pos);

    if (shield_halo_->getScale().x < 1.001 && shield_halo_->getScale().x > 0.999)
        shield_halo_->setScale(getWidth()/460.,
               getHeight()/460.);

    //subSystems_.at(HULL)->update();
    //shared_ptr<Armor>::(subSystems_.at(ARMOR)->update());
    //shared_ptr<Shield>::(subSystems_.at(SHIELD)->update());
    //shared_ptr<GravitationalStabilizer>::(subSystems_.at(STABILIZER)->update());
    //updating subsystems?
    for (unsigned int i = 0; i < subSystems_.size(); ++i)
    {
        //TODO wtf does this do?
        subSystems_.at(i)->update();
    }

    moving_ = NONE;
}

void Ship::move(Direction dir)
{
    moving_ = dir;
}

void Ship::movement()
{
    // translating controllers command to move into a force to to move the ship
    switch(moving_)
    {
    case UP:
    {
        sf::Vector2f force(0.0, -enginePower_);
        actForce(force);
    }
    break;
    case DOWN:
    {
        sf::Vector2f force(0.0, enginePower_);
        actForce(force);
    }
    break;
    case LEFT:
    {
        sf::Vector2f force(-enginePower_, 0.0);
        actForce(force);
    }
    break;
    case RIGHT:
    {
        sf::Vector2f force(enginePower_, 0.0);
        actForce(force);
    }
    break;
    case UPLEFT:
    {
        sf::Vector2f force(-enginePower_, -enginePower_);
        actForce(force);
    }
    break;
    case UPRIGHT:
    {
        sf::Vector2f force(enginePower_, -enginePower_);
        actForce(force);
    }
    break;
    case DOWNLEFT:
    {
        sf::Vector2f force(-enginePower_,  enginePower_);
        actForce(force);
    }
    break;
    case DOWNRIGHT:
    {
        sf::Vector2f force(enginePower_, enginePower_);
        actForce(force);
    }
    break;
    default:
    {
    }
    break;
    }
}

void Ship::shoot(bool is_shooting, sf::Vector2f target_pos)
{
    //updating weapon for loading purposes
    if(weapons_.size()-1 >= current_weapon_)
    {
        weapons_.at(current_weapon_)->update();
        if (is_shooting && getSubSystems().at(ENERGY)->getHealth() > 0)
        {
            //TODO weapons take more enery!!
            if (weapons_.at(current_weapon_)->getLoadingTimeRemaining() == 0 &&
                current_weapon_ != ROCKET)
            getSubSystems().at(ENERGY)->doDamage(1);
            weapons_.at(current_weapon_)->shoot(target_pos);
        }
    }
}

void Ship::addWeapon(shared_ptr<Weapon> weapon)
{
    weapons_.push_back(weapon);
}

void Ship::doDamage(unsigned int damage) {
    if (getSubSystems().at(SHIELD)->getHealth() < damage) {
        if (getSubSystems().at(ARMOR)->getHealth() == 0) {
            unsigned int chosen = std::rand()%subSystems_.size();
            getSubSystems().at(chosen)->doDamage(damage);
        } else {
            getSubSystems().at(ARMOR)->doDamage(damage);
        }
    } else {
        getSubSystems().at(SHIELD)->doDamage(damage);
    }
}

unsigned int Ship::getReloadTimeRemaining () {
    return weapons_.at(current_weapon_)->getLoadingTimeRemaining();
}

unsigned int Ship::getReloadTime () {
     return weapons_.at(current_weapon_)->getLoadingTime();
}

void Ship::draw(sf::RenderWindow& window, double interpolation) {
    DrawableObject::draw(window,interpolation);
    //MovingObject::draw(window,interpolation);

    if (getSubSystems().at(SHIELD)->getHealth() > 0) {
        window.draw(*shield_halo_);
    }
}
