#include <Graphics.h>
#include "HumanController.h"



#include "Config.h"

namespace {
   void drawHelper(std::pair<const unsigned int, std::tr1::shared_ptr<DrawableObject> > i) {
       i.second->draw((*Graphics::inst()->getWindow()), Graphics::inst()->getInterpolation());
       /*if (ObjectFactory::inst()->getDrawableObjects()->find(i.first) != ObjectFactory::inst()->getDrawableObjects()->end()) {
           ObjectFactory::inst()->getDrawableObjects()->at(i.first)->draw(*(Graphics::inst()->getWindow()));
       }*/
   }
}

Graphics* Graphics::instance_ = 0;

Graphics::Graphics()
{
    //ctor
}

Graphics::~Graphics()
{
    //dtor
}

Graphics* Graphics::inst()
{
    if(instance_ == 0)
    {
        instance_ = new Graphics();
    }

    return instance_;
}

void Graphics::init()
{
    //sf::VideoMode::getFullscreenModes();
    //sf::VideoMode(800, 600)


    //making window
    std::cout << "Graphics init: " << "setting view size\n";
    shared_ptr<sf::View> view(new sf::View);
    window_view_ = view;
    window_view_->reset( sf::FloatRect(0,0,WIDTH, HEIGHT) );
      std::cout << "Graphics init: " << "initializing window\n";
    //TODO add sf::Style::Fullscreen as last parameter if full screen is necessary
    shared_ptr<sf::RenderWindow> window(new sf::RenderWindow(sf::VideoMode(WIDTH, HEIGHT),"SFML works!"));
      std::cout << "Graphics init: " << "setting view\n";
    window->setView(*window_view_);
    std::cout << "Graphics init: " << "assigning windown\n";
    window_ = window;
    //removing mouse cursor
    window_->setMouseCursorVisible (false);
    //REMOVE
    //random shape
    shared_ptr<sf::CircleShape> shape(new sf::CircleShape(100.f));
    shape_ = shape;
    shape_->setFillColor(sf::Color::Green);

    //initializing crosshair sprite
    //first loading texture
    string texture_file = "textures/crosshair.png";
    std::cout << "Graphics init: " << "loading crosshair texture\n";
    if (texture_.loadFromFile(texture_file)) {
        std::cout << "Graphics init: " << "making sprite with crosshair texture\n";
        shared_ptr<sf::Sprite> sprite(new sf::Sprite(texture_));
        setCrosshairTexture(texture_);
        //TODO calculate scale from the window
        sprite->setScale(0.25,0.25);
        std::cout << "Graphics init: " << "assigning sprite\n";
        crosshair_ = sprite;

    } else {
        shared_ptr<sf::Sprite> sprite(new sf::Sprite());
        sprite->setColor(sf::Color::Green);
        //TODO calculate scale from the window
        sprite->setScale(0.25,0.25);

        crosshair_ = sprite;
    }
}

void Graphics::cleanup () {}

void Graphics::draw (double interpolation)
{
    interpolation_ = interpolation;
    window_->clear(sf::Color(10,10,50,200));

    windowTransform();

    /*for(std::map<unsigned int, PhysicalObject>::iterator it =
        ObjectFactory::inst()->getDrawableObjects()->begin();
        it != ObjectFactory::inst()->getMovingObjects()->end(); ++it) {

        }*/

    std::for_each(ObjectFactory::inst()->getDrawableObjects()->begin(),
                  ObjectFactory::inst()->getDrawableObjects()->end(),
                  drawHelper);

    //after drawing objects we draw crosshair
    sf::Mouse mouse;
    sf::Vector2i mouse_pos = mouse.getPosition(*window_);

    //TODO position must be corrected by window size window_->getSize()
    mouse_pos.x -= 20*Graphics::inst()->getWindow()->getSize().x/1920;
    mouse_pos.y -= 20*Graphics::inst()->getWindow()->getSize().y/1080;
    crosshair_->setPosition(Graphics::inst()->getWindow()->convertCoords(mouse_pos));
    window_->draw(*crosshair_);

    //drawing human ships health bar
    ObjectFactory::inst()->getHealthBar()->draw();

    for(unsigned int i = 0; i < texts_.size(); ++i)
    {
        window_->draw(*(texts_.at(i)));
    }

    //ship_->draw(*window_);
    window_->display();
}

void Graphics::windowTransform() {
    sf::Vector2f pos = ObjectFactory::inst()->getPlayer()->getShip()->getPosition();
    //corrects window position a bit
    pos.x += WIDTH/6;
    pos.y += HEIGHT/4;
    sf::Vector2f temp = pos;
    if (pos.x-WIDTH/2 < 0) {
        pos.x = 0;
        temp.x = WIDTH/2;
    }
    else if(pos.x + WIDTH/2 > AREA.x)
    {
        pos.x = AREA.x - WIDTH/2;
        temp.x = AREA.x - WIDTH/2;
    }
    if (pos.y-HEIGHT/2 < 0) {
        pos.y = 0;
        temp.y = HEIGHT/2;
    }
    else if(pos.y + HEIGHT/2 > AREA.y)
    {
        pos.y = AREA.y - HEIGHT/2;
        temp.y = AREA.y - HEIGHT/2;
    }

    window_view_->setCenter(temp.x,temp.y);
    window_->setView(*window_view_);
}
