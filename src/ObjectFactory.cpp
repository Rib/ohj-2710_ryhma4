#include "ObjectFactory.h"


#include "StaticObject.h"
#include "HumanController.h"
#include "AiController.h"
#include "Level.h"
#include "FirstLevel.h"
#include "Config.h"
#include "Rocket.h"
#include "PlasmaBall.h"
#include "Laser.h"
#include "SoundManager.h"

ObjectFactory* ObjectFactory::instance_ = 0;

ObjectFactory::ObjectFactory()
    :
//    instance_(),
    first_(true),
    player_()

{
    //init();
}

ObjectFactory::~ObjectFactory()
{
    //dtor
}

void ObjectFactory::init()
{
    drawable_objects_.reset(new std::map<unsigned int, shared_ptr<DrawableObject> >);
    moving_objects_.reset(new std::map<unsigned int, shared_ptr<PhysicalObject> >);
    //static_objects_.reset(new std::map<unsigned int, shared_ptr<DrawableObject> >);
    controllers_.reset(new std::vector<shared_ptr<Controller> >);
    projectile_ = SoundManager::inst()->loadSound("sounds/projectile.ogg");
    rocket_ = SoundManager::inst()->loadSound("sounds/rocket.ogg");
    laser_ = SoundManager::inst()->loadSound("sounds/laser.ogg");
    plasma_ = SoundManager::inst()->loadSound("sounds/plasma.ogg");
}

void ObjectFactory::cleanup()
{
}


void ObjectFactory::createGame()
{
    std::cout << "Creating game" << std::endl;
    if(!level_) // also creates human
    {
        level_.reset(new FirstLevel);
        level_->init();
    }
    else
    {
        level_->cleanup();
        level_->init();
    }

    std::cout << "Game created" << std::endl;
}

void ObjectFactory::createHuman()
{
    player_.reset(new HumanController());
    std::cout << "CreateHuman: " << "Init\n";
    controllers_->push_back(player_);
    player_->setShip(createShip(true));
    std::cout << "CreateHuman: " << "Creating human ship\n";
    player_->getShip()->loadTexture("textures/ship3.png", 0.25);
    player_->getShip()->setPosition(STARTPOSITION);

    player_->getShip()->getSubSystems().at(HULL)->setHealth(SHIP_HULL_HEALTH);
    player_->getShip()->getSubSystems().at(ARMOR)->setHealth(SHIP_ARMOR_HEALTH);
    player_->getShip()->getSubSystems().at(SHIELD)->setHealth(SHIP_SHIELD_HEALTH);
    player_->getShip()->getSubSystems().at(STABILIZER)->setHealth(SHIP_GRAV_STAB_HEALTH);
    player_->getShip()->getSubSystems().at(ENERGY)->setHealth(SHIP_ENERGY);

    player_->getShip()->setMass(PLAYER_MASS);

    armShip(std::tr1::shared_ptr<Weapon>(new Weapon()), player_->getShip());
    armRocket(std::tr1::shared_ptr<Weapon>(new Weapon(false)), player_->getShip());
    armLaser(std::tr1::shared_ptr<Weapon>(new Weapon()), player_->getShip());




    //making healthbar for human
    health_bar_.reset(new HealthBar());
    health_bar_->init(player_->getShip()->getId());
}

shared_ptr<AiController> ObjectFactory::createEnemy(std::string texture, float scale, bool boss)
{
    shared_ptr<AiController> enemy(new AiController(boss));
    getControllers()->push_back(enemy);
    enemy->setShip(createShip());
    enemy->getShip()->loadTexture(texture, scale);
    enemy->setTarget(getPlayer()->getShip());

    enemy->getShip()->getSubSystems().at(HULL)->setHealth(!boss ? ENEMY_BASIC_SHIP_HULL_HEALTH:
                                                        ENEMY_BOSS_SHIP_HULL_HEALTH);
    enemy->getShip()->getSubSystems().at(ARMOR)->setHealth(!boss ? ENEMY_BASIC_SHIP_ARMOR_HEALTH:
                                                         ENEMY_BOSS_SHIP_ARMOR_HEALTH);
    enemy->getShip()->getSubSystems().at(SHIELD)->setHealth(!boss ? ENEMY_BASIC_SHIP_SHIELD_HEALTH:
                                                          ENEMY_BOSS_SHIP_SHIELD_HEALTH);
    enemy->getShip()->getSubSystems().at(STABILIZER)->setHealth(!boss ? ENEMY_BASIC_SHIP_GRAV_STAB_HEALTH:
                                                              ENEMY_BOSS_SHIP_GRAV_STAB_HEALTH);
    enemy->getShip()->getSubSystems().at(ENERGY)->setHealth(!boss ? ENEMY_BASIC_SHIP_ENERGY:
                                                          ENEMY_BOSS_SHIP_ENERGY);

    enemy->getShip()->setMass(!boss ? ENEMY_BASIC_SHIP_MASS : ENEMY_BOSS_MASS);

    return enemy;
}

ObjectFactory* ObjectFactory::inst()
{
    if(instance_ == 0)
    {
        instance_ = new ObjectFactory();
    }

    return instance_;
}

shared_ptr<Ship> ObjectFactory::createShip(bool is_player)
{
    //creating and storing new ship
    shared_ptr<Ship> ship(new Ship());
    ship->init();

    if (is_player) ship->setId(0);
    drawable_objects_->insert(std::pair<unsigned int, shared_ptr<DrawableObject> >(ship->getId(), ship));
    moving_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(ship->getId(), ship));

    return ship;
}

shared_ptr<Projectile> ObjectFactory::createProjectile(sf::Vector2f from, sf::Vector2f to)
{
    shared_ptr<Projectile> projectile(new Projectile());
    projectile->init(from, to);
    drawable_objects_->insert(std::pair<unsigned int, shared_ptr<DrawableObject> >(projectile->getId(), projectile));
    moving_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(projectile->getId(), projectile));

    return projectile;
}

void ObjectFactory::createPlasma(sf::Vector2f from, sf::Vector2f to)
{
    shared_ptr<PlasmaBall> plasma(new PlasmaBall());
    plasma->init(from, to);
    drawable_objects_->insert(std::pair<unsigned int, shared_ptr<DrawableObject> >(plasma->getId(), plasma));
    moving_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(plasma->getId(), plasma));

}

void ObjectFactory::createRocket(sf::Vector2f position, Movement& m, bool left)
{
    shared_ptr<Rocket> rocket(new Rocket);
    rocket->init(left);
    if(m.direction.y < 0)
    {
        m.direction.y = 0;
    }
    rocket->setMovement(m);
    rocket->setPosition(position);
    drawable_objects_->insert(std::pair<unsigned int, shared_ptr<DrawableObject> >(rocket->getId(), rocket));
    moving_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(rocket->getId(), rocket));
}

void ObjectFactory::createLaser(sf::Vector2f from, sf::Vector2f to){
    shared_ptr<Laser> laser(new Laser());
    laser->init(from, to);
    drawable_objects_->insert(std::pair<unsigned int, shared_ptr<DrawableObject> >(laser->getId(), laser));
    moving_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(laser->getId(), laser));
}

shared_ptr<StaticObject> ObjectFactory::createGround() {
        shared_ptr<StaticObject> ground(new StaticObject);
        ground->init();

        drawable_objects_->insert(std::pair<unsigned int, shared_ptr<DrawableObject> >(ground->getId(), ground));
        //static_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(ground->getId(), ground));
        //moving_objects_->insert(std::pair<unsigned int, shared_ptr<PhysicalObject> >(ground->getId(), ground));
        // (*ObjectFactory::inst()->getDrawableObjects())[ground->getId()] = ground;
        // (*ObjectFactory::inst()->getStaticObjects())[ground->getId()] = ground;
        return ground;
}

void ObjectFactory::destroyObject(shared_ptr<DrawableObject> object)
{
    std::cout << "destroying object and possibly controller with controlling Id: " <<  object->getId() << std::endl;
    // delete contoller
    for(unsigned int i = 0; i < controllers_->size(); ++i)
    {
        if(object->getId() == controllers_->at(i)->getShip()->getId())
        {
            if(controllers_->at(i)->getShip()->getId() == player_->getShip()->getId())
            {
                player_.reset();
            }
            // delete i:th element
            controllers_->erase(controllers_->begin() + i);

            std::cout << "Deleted controller with controlling Id: " <<  object->getId() << std::endl;
            --i;
        }
    }

    std::cout << "Deleting object with Id: " <<  object->getId() << std::endl;
    drawable_objects_->erase(object->getId());
    moving_objects_->erase(object->getId());
    //static_objects_->erase(object->getId());

    // ei kaadu koska parametrina shared ptr
    std::cout << "Deleted object with Id: " <<  object->getId() << std::endl;
}

bool ObjectFactory::update()
{
    if(level_->update() && first_)
    {
        level_->cleanup();
        level_.reset(new Level);
        level_->init();
        return true;
    }

    return false;
}


shared_ptr<Ship> ObjectFactory::getPlayerShip()
{
    return getPlayer()->getShip();
}

void ObjectFactory::armShip(shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship)
{
    weapon->init(ship, PROJECTILE, 1, 5, 45, projectile_);
    ship->addWeapon(weapon);
}

void ObjectFactory::armRocket(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship)
{
    weapon->init(ship, ROCKET, 10, 20, 120, rocket_);
    ship->addWeapon(weapon);
}

void ObjectFactory::armLaser(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship) {
    weapon->init(ship, LASER, 1, 5, 1, laser_);
    ship->addWeapon(weapon);
}

void ObjectFactory::armPlasma(std::tr1::shared_ptr<Weapon> weapon, std::tr1::shared_ptr<Ship> ship)
{
    weapon->init(ship, PLASMA, 30, 5, 120, plasma_);
    ship->addWeapon(weapon);
}
