#include "HealthBar.h"

#include <sstream>

HealthBar::HealthBar()
{
    //ctor
}

HealthBar::~HealthBar()
{
    //dtor
}

namespace {
    std::string int2str (unsigned int tr) {
        std::string return_val;
        std::ostringstream oss;
        oss << tr;
        return_val = oss.str();
        return return_val;
    }
}

void HealthBar::init(unsigned int ship_init) {
    std::cout << "Healthbar init\n";
    //initializing health bar
    health_bar_.reset(new sf::RectangleShape());
    std::cout << "Bar Ship init: " << " new\n";
    health_bar_->setOutlineColor(sf::Color::Red);
    health_bar_->setOutlineThickness(2);
    std::cout << "Bar Ship init: " << " color\n";
    health_bar_->setSize(HEALTHBARSIZE);
    std::cout << "Bar Ship init: " << " size\n";
    health_bar_->setPosition(ObjectFactory::inst()->getPlayerShip()->getPosition());
    std::cout << "Bar Ship init: " << " position\n";

    std::cout << "Bar Subsystems init\n";
    unsigned int sub_system_amount = ObjectFactory::inst()->getPlayerShip()->getSubSystems().size();
    //TODO names to our bars
    for (unsigned int i = 0; i < sub_system_amount; ++i) {
        shared_ptr<sf::Text> p;
        shared_ptr<sf::RectangleShape> r;
        shared_ptr<sf::RectangleShape> r2;
        r.reset(new sf::RectangleShape());
        r2.reset(new sf::RectangleShape());

        sub_system_text_.push_back(p);
        sub_system_bar_.push_back(r);
        red_bar_.push_back(r2);

        sub_system_text_.at(i).reset(new sf::Text());
        //configuring texts
        sub_system_text_.at(i)->setString(Subsystem_name[i]);
        sub_system_text_.at(i)->setCharacterSize(25*Graphics::inst()->getWindow()->getSize().x/1920);
        sub_system_text_.at(i)->setColor(sf::Color::White);

        //configuring red bar
        if (i != ENERGY)
        red_bar_.at(i)->setFillColor(sf::Color::Red);
        else
        red_bar_.at(i)->setFillColor(sf::Color::Transparent);
        red_bar_.at(i)->setSize(HEALTHBARSIZE);

        //configuing bars
        if ((i != ENERGY && i != SHIELD) || (i == SHIELD &&
            ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(SHIELD)->isDisabled()))
        sub_system_bar_.at(i)->setFillColor(sf::Color::Green);
        else
        sub_system_bar_.at(i)->setFillColor(sf::Color::Cyan);
        sub_system_bar_.at(i)->setSize(HEALTHBARSIZE);
    }

    std::cout << "Bar Weapons init\n";
    unsigned int weapons_amount = ObjectFactory::inst()->getPlayerShip()->getWeapons().size();

    //initializing weapon textures
    for (unsigned int i = 0; i < weapons_amount; ++i) {
        shared_ptr<sf::Text> number;
        shared_ptr<sf::Sprite> weapon;
        shared_ptr<sf::Texture> texture;

        weapon_text_.push_back(number);
        weapon_pictures_.push_back(weapon);
        weapon_textures_.push_back(texture);
        weapon_text_.at(i).reset(new sf::Text);
        if (i == ROCKET)
        weapon_text_.at(i)->setString(int2str(ObjectFactory::inst()->getPlayerShip()->
                                              getWeapons().at(ROCKET)->getClipLeft()));
        else
        weapon_text_.at(i)->setString("INF");
        weapon_text_.at(i)->setCharacterSize(30*Graphics::inst()->getWindow()->getSize().x/1920);
        weapon_text_.at(i)->setColor(sf::Color::White);

        weapon_pictures_.at(i).reset(new sf::Sprite);
        weapon_textures_.at(i).reset(new sf::Texture);

        loadWeaponTexture(Weapon_textures[i], i);
    }

    //setting current weapons rectangl
    selected_weapon_.reset(new sf::RectangleShape);
    selected_weapon_->setOutlineColor(sf::Color::Yellow);
    selected_weapon_->setOutlineThickness(2);
    selected_weapon_->setFillColor(sf::Color::Transparent);
    sf::Vector2f size((HEALTHBARSIZE.x+60)*Graphics::inst()->getWindow()->getSize().x/1920,
                      (HEALTHBARSIZE.y+20)*Graphics::inst()->getWindow()->getSize().y/1080);
    selected_weapon_->setSize(size);

    std::cout << "Bar Reload Bar init\n";
    reload_text_.reset(new sf::Text);
    reload_text_->setString("Reload");
    reload_text_->setCharacterSize(25*Graphics::inst()->getWindow()->getSize().x/1920);
    reload_text_->setColor(sf::Color::White);

    reload_bar_.reset(new sf::RectangleShape);
    reload_bar_->setFillColor(sf::Color::Blue);
    sf::Vector2f size_2((HEALTHBARSIZE.x)*Graphics::inst()->getWindow()->getSize().x/1920,
                      HEALTHBARSIZE.y*Graphics::inst()->getWindow()->getSize().y/1080);
    reload_bar_->setSize(size_2);

    std::cout << "Bar Complete\n";
}

void HealthBar::draw()
{
    sf::Vector2i conv;
    conv.x = 0;
    conv.y = Graphics::inst()->getWindow()->getSize().y;
    conv.y -= 50*Graphics::inst()->getWindow()->getSize().x/1920;

    //setting subsystem bars to right position
    unsigned int sub_system_amount = sub_system_text_.size();
    for (unsigned int i = 0; i < sub_system_amount; ++i) {
        conv.x += 150*Graphics::inst()->getWindow()->getSize().x/1920;
        sub_system_text_.at(i)->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));
        Graphics::inst()->getWindow()->draw(*sub_system_text_.at(i));

        conv.y -= 110*Graphics::inst()->getWindow()->getSize().y/1080;
        //drawing red bar
        red_bar_.at(i)->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));
        Graphics::inst()->getWindow()->draw(*red_bar_.at(i));

        //drawing green bar
        sub_system_bar_.at(i)->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));
        sf::Vector2f size(HEALTHBARSIZE.x,
                          (HEALTHBARSIZE.y*ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(i)->getHealth())/
                           ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(i)->getMaxHealth());
        sub_system_bar_.at(i)->setSize(size);
        if (i == SHIELD &&
            !ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(SHIELD)->isDisabled()) {
                sub_system_bar_.at(i)->setFillColor(sf::Color::Cyan);
            } else if (i == SHIELD &&
            ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(SHIELD)->isDisabled()){
                sub_system_bar_.at(i)->setFillColor(sf::Color::Green);
            }

        Graphics::inst()->getWindow()->draw(*sub_system_bar_.at(i));
        conv.y += 110*Graphics::inst()->getWindow()->getSize().y/1080;

    }

    //setting wepons to their correct positions
    conv.x += 150*Graphics::inst()->getWindow()->getSize().x/1920;
    unsigned int weapons_count = weapon_text_.size();

    for (unsigned int i = 0; i < weapons_count; ++i) {
        conv.x += 150*Graphics::inst()->getWindow()->getSize().x/1920;
        conv.y -= 50*Graphics::inst()->getWindow()->getSize().y/1080;
        weapon_text_.at(i)->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));


        conv.y -= 50*Graphics::inst()->getWindow()->getSize().y/1080;
        weapon_pictures_.at(i)->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));


        if (i == ObjectFactory::inst()->getPlayerShip()->getCurrentWeapon()) {
            conv.x -= 10*Graphics::inst()->getWindow()->getSize().x/1920;
            conv.y -= 10*Graphics::inst()->getWindow()->getSize().y/1080;
            selected_weapon_->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));
            Graphics::inst()->getWindow()->draw(*selected_weapon_);
            conv.x += 10*Graphics::inst()->getWindow()->getSize().x/1920;
            conv.y += 10*Graphics::inst()->getWindow()->getSize().y/1080;
        }

        //looking amount of rockets in the clip
        if (i == ROCKET)
        weapon_text_.at(i)->setString(int2str(ObjectFactory::inst()->getPlayerShip()->
                                              getWeapons().at(ROCKET)->getClipLeft()));

        Graphics::inst()->getWindow()->draw(*weapon_text_.at(i));
        Graphics::inst()->getWindow()->draw(*weapon_pictures_.at(i));

        conv.y += 100*Graphics::inst()->getWindow()->getSize().y/1080;
    }


    //setting reload stuff to right position
    conv.x = Graphics::inst()->getWindow()->getSize().x - 100*Graphics::inst()->getWindow()->getSize().x/1920;
    conv.y = Graphics::inst()->getWindow()->getSize().y - 50*Graphics::inst()->getWindow()->getSize().y/1080;
    //setting reload bar to right position
    reload_text_->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));
    //drawing the reload text
    Graphics::inst()->getWindow()->draw(*reload_text_);

    conv.y -= 110*Graphics::inst()->getWindow()->getSize().y/1080;
    reload_bar_->setPosition(Graphics::inst()->getWindow()->convertCoords(conv));
    if (ObjectFactory::inst()->getPlayerShip()->getReloadTimeRemaining() == 0) {
            reload_bar_->setFillColor(sf::Color::Blue);
            reload_bar_->setSize(HEALTHBARSIZE);
        } else {
            reload_bar_->setFillColor(sf::Color::Yellow);
            sf::Vector2f size(HEALTHBARSIZE.x,
                (static_cast<float>(ObjectFactory::inst()->getPlayerShip()->getReloadTime() -
                  ObjectFactory::inst()->getPlayerShip()->getReloadTimeRemaining())/
                 static_cast<float>(ObjectFactory::inst()->getPlayerShip()->getReloadTime()))*
                HEALTHBARSIZE.y);
            reload_bar_->setSize(size);
        }
    //drawing the reload bar
    Graphics::inst()->getWindow()->draw(*reload_bar_);

    /*//TODO constant for max health or something
    sf::Vector2f size((ObjectFactory::inst()->getPlayerShip()->getDamage()/100.)*
                      ObjectFactory::inst()->getPlayerShip()->getWidth(), 3);
    health_bar_->setSize(size);

    sf::Vector2f pos = ObjectFactory::inst()->getPlayerShip()->getPosition();
    pos.y -= 10;
    health_bar_->setPosition(pos);

    Graphics::inst()->getWindow()->draw(*health_bar_);*/
}

void HealthBar::loadWeaponTexture(const std::string& filename, unsigned int weapon) {

     weapon_textures_.at(weapon)->loadFromFile(filename);

    //std::cout << "Load texture: " << " creating sprite\n";
    weapon_pictures_.at(weapon)->setTexture(*weapon_textures_.at(weapon));

    weapon_pictures_.at(weapon)->setScale(0.35*Graphics::inst()->getWindow()->getSize().x/1920,0.35*Graphics::inst()->getWindow()->getSize().y/1080);

}

