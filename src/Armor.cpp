#include "Armor.h"

#include "Config.h"

Armor::Armor()
{
    //ctor
}

Armor::~Armor()
{
    //dtor
}

void Armor::update()
{
    if (std::rand()%ARMOR_REGENERATION_RATE == 0) repair();
    Subsystem::update();
}

