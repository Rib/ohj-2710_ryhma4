#include "HumanController.h"

HumanController::HumanController()
    :
    shieldKeyDown_(false),
    switchKeyDown_(false),
    wantsToQuit_(false)
{
    //ctor
}

HumanController::~HumanController()
{
    //dtor
}


void HumanController::control()
{

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
    {
        wantsToQuit_ = true;
    }
    movement();
    shooting();

}

bool HumanController::wantsToQuit()
{
    return wantsToQuit_;
}


void HumanController::movement()
{
    bool up = false;
    bool down = false;
    bool left = false;
    bool right = false;

    // define what keys are which direction
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        left = true;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        right = true;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        up = true;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        down = true;
    }

    Direction dir = NONE;
    // is movent directed to somewhere left

    if(left && !right)
    {

        if(up && !down)
        {
            dir = UPLEFT;
        }
        else if(!up && down)
        {
            dir = DOWNLEFT;
        }
        else
        {
            dir = LEFT;
        }
    }
    // are we moving right in some way
    else if(!left && right)
    {
        if(up && !down)
        {
            dir = UPRIGHT;
        }
        else if(!up && down)
        {
            dir = DOWNRIGHT;
        }
        else
        {
            dir = RIGHT;
        }
    }
    // directly up
    else if(up && !down)
    {
        dir = UP;
    }
    // directly down
    else if(!up && down)
    {
        dir = DOWN;
    }

    getShip().get()->move(dir);

    //weapon switches
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
    {
        getShip()->setCurrentWeapon(0);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
    {
        getShip()->setCurrentWeapon(1);
        //TODO second weapon switch
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
    {
        getShip()->setCurrentWeapon(2);
        //TODO third weapon switch
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        if(!switchKeyDown_)
        {
        unsigned int current = getShip()->getCurrentWeapon();
        ++current;
        if (current == getShip()->getWeapons().size())
        {
            current = 0;
        }
        getShip()->setCurrentWeapon(current);
        //TODO third weapon switch

        switchKeyDown_ = true;
        }
    }
    else
    {
        switchKeyDown_ = false;
    }

    //repair key R
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
    {
        unsigned int chosen = std::rand()%4;
        if (chosen != SHIELD &&
                ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(ENERGY)->getHealth() >= 3 &&
                ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(chosen)->getHealth() <
                ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(chosen)->getMaxHealth() )
        {
            ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(ENERGY)->doDamage(3);
            ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(chosen)->repair();
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
    {
        if(!shieldKeyDown_)
        {
            if (ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(SHIELD)->isDisabled())
            {
                ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(SHIELD)->disable(false);
            }
            else
            {
                ObjectFactory::inst()->getPlayerShip()->getSubSystems().at(SHIELD)->disable(true);
            }
        }
        shieldKeyDown_ = true;
    }
    else
    {
        shieldKeyDown_ = false;
    }
}

void HumanController::shooting()
{
    bool is_shooting = false;
    //if we have pressed left mouse button we shoot
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        is_shooting = true;
    }

    sf::Vector2f mouse = Graphics::inst()->getWindow()->convertCoords(sf::Mouse::getPosition(*Graphics::inst()->getWindow()));
    //mouse.x += 20;
    //mouse.y += 20;

    //mouse.x = sf::Mouse::getPosition().x + transform.x - Graphics::inst()->getWindow()->getPosition().x;
    //mouse.y = sf::Mouse::getPosition().y + transform.y - Graphics::inst()->getWindow()->getPosition().y;
    getShip().get()->shoot(is_shooting, mouse);
}
