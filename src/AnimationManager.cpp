#include "AnimationManager.h"


#include "Animation.h"
#include "ObjectFactory.h"

AnimationManager* AnimationManager::instance_ = 0;


AnimationManager::AnimationManager()
{
    //ctor
}

AnimationManager::~AnimationManager()
{
    //dtor
}

AnimationManager* AnimationManager::inst()
{
    if(instance_ == 0)
    {
        instance_ = new AnimationManager();
    }

    return instance_;
}

void AnimationManager::playAnimation(std::string file, sf::Vector2f position, unsigned int images,
                                     unsigned int frameTime)
{
    std::tr1::shared_ptr<Animation> a(new Animation(images, frameTime));
    a->loadTexture(file);
    a->setPosition(position);
    (*ObjectFactory::inst()->getDrawableObjects())[a->getId()] = a;

    animations_.push_back(a);
}

void AnimationManager::update()
{
    for(std::list<std::tr1::shared_ptr<Animation> >::iterator i = animations_.begin(); i != animations_.end(); ++i)
    {
        if(!(*i)->animationUpdate())
        {
            std::list<std::tr1::shared_ptr<Animation> >::iterator temp = i;
            --i;
            ObjectFactory::inst()->destroyObject((*temp));
            animations_.erase(temp);
        }
    }
}
