#include "FirstLevel.h"

#include "DrawableObject.h"
#include "StaticObject.h"
#include "ObjectFactory.h"
#include "AiController.h"
#include "HumanController.h"
#include "SoundManager.h"

#include "Config.h"
FirstLevel::FirstLevel()
{
    //ctor
}

FirstLevel::~FirstLevel()
{
    //dtor
}


void FirstLevel::init()
{
    std::cout << "First level init: " << std::endl;

    SoundManager::inst()->playMusic("sounds/firstlevel.wav");
    ObjectFactory::inst()->createHuman();


    for (unsigned int i = 0; i< CLOUDS; ++i)
    {
        shared_ptr<DrawableObject> cloud(new DrawableObject);
        (*ObjectFactory::inst()->getDrawableObjects())[cloud->getId()] = cloud;
        sf::Vector2f start(rand()%AREA.x, rand()%AREA.y-400);
        cloud->setPosition(start);
        //std::cout << "Stars init: " << "new star added to" << start.x <<"," << start.y << std::endl;
        cloud->loadTexture("textures/cloud_1.png", 0.5);
    }

    unsigned int xLoc = 0;
    std::vector<std::string> textures;
    textures.push_back("textures/redRock1.png");
    textures.push_back("textures/redRock2.png");
    textures.push_back("textures/redRock3.png");
    textures.push_back("textures/redRock4.png");

    std::cout << "Level init: " << "creating ground" << std::endl;
    while (xLoc < AREA.x)
    {
        shared_ptr<StaticObject> ground (ObjectFactory::inst()->createGround());

        unsigned int tIndex = rand()%textures.size();
        ground->loadTexture(textures.at(tIndex), 1);

        ground->setPosition(sf::Vector2f(xLoc, AREA.y - ground->getHeight()));

        xLoc += ground->getWidth();

    }

    xLoc = ENEMYSTART;

    for(unsigned int i = 0;  i < FIRST_LEVEL_ENEMIES; ++i)
    {
        if(rand()%4 == 0)
        {
            shared_ptr<AiController> enemy = ObjectFactory::inst()->createEnemy("textures/ship2.png", 0.5);

            enemy->getShip()->setPosition(sf::Vector2f(xLoc, AREA.y/2 + rand()%401 - 200));
            enemy->getShip()->setEnginePower(ENEMYENGINEPOWER);

            ObjectFactory::inst()->armRocket(shared_ptr<Weapon>(new Weapon()), enemy->getShip());
/*
            for (unsigned int i = 0; i < enemy->getShip()->getSubSystems().size(); ++i)
            {
                enemy->getShip()->getSubSystems().at(i)->setHealth(enemy->getShip()->getSubSystems().at(i)->getHealth()/4);
            }*/
        }
        else
        {
            shared_ptr<AiController> enemy = ObjectFactory::inst()->createEnemy("textures/enemyship.png", 0.5);

            enemy->getShip()->setPosition(sf::Vector2f(xLoc, AREA.y/2 + rand()%401 - 200));
            enemy->getShip()->setEnginePower(ENEMYENGINEPOWER);

            ObjectFactory::inst()->armShip(shared_ptr<Weapon>(new Weapon()), enemy->getShip());

            for (unsigned int i = 0; i < enemy->getShip()->getSubSystems().size(); ++i)
            {
                enemy->getShip()->getSubSystems().at(i)->setHealth(enemy->getShip()->getSubSystems().at(i)->getHealth()/4);
            }

        }
        xLoc += (AREA.x - ENEMYSTART- BOSSZONE)/FIRST_LEVEL_ENEMIES;
    }

    //enemy->getShip()->setCurrentWeapon(0);

    shared_ptr<DrawableObject> portal(new DrawableObject);
    (*ObjectFactory::inst()->getDrawableObjects())[portal->getId()] = portal;
    portal->loadTexture("textures/portal.png", 2);

    portal->setPosition(sf::Vector2f(AREA.x- portal->getWidth(), AREA.y- portal->getHeight() -192 ));
    portal_ = portal->getPosition();


}


bool FirstLevel::update()
{
    /*
    if(ObjectFactory::inst()->getMovingObjects()->find(bossID_) == ObjectFactory::inst()->getMovingObjects()->end() && !won_)
    {
        shared_ptr<sf::Text> victory(new sf::Text("And so the evil *insert empire name here* falls and the multiverse is a safe place once more."));
        victory->setPosition(ObjectFactory::inst()->getPlayer()->getShip()->getPosition());
        Graphics::inst()->addText(victory);

        won_ = true;
        return true;
    }*/
    sf::Vector2f position = ObjectFactory::inst()->getPlayer()->getShip()->getPosition();
    if(position.x > portal_.x && position.y > portal_.y)
    {
        return true;
    }


    return false;
}
